import express from 'express';
import { copyFile } from '../musicProcessor';
import * as jsonpack from 'jsonpack';

import * as PostDB from './postDB';
import * as UserDB from './userDB';
import * as MusicDB from './musicDB';

const router = express.Router();
export default router;

router.post('/post', (req, res) => {
	if (jsonpack.unpack(req.body).song) {
		MusicDB.getSong(jsonpack.unpack(req.body).song, song => {
			song = '/instruments/custom/' + req.user._id + '/' + song.song.track.id;
			let m = new Date().getTime();

			copyFile('.\\public' + song.replace(/\//g, '\\') + '.wav', '.\\public' + song.replace(/\//g, '\\') + m + '.wav', err => {
				if (err) return console.log(err);
				PostDB.addPost(req.user._id, jsonpack.unpack(req.body).text, song + m + '.wav', status => res.json(status));
			});
		});
	} else PostDB.addPost(req.user._id, jsonpack.unpack(req.body).text, null, status => res.json(status));
});

router.post('/comment', (req, res) => {
	PostDB.getPost(jsonpack.unpack(req.body).postId, post => {
		if (!post) res.json(null);

		if (!post.comments) Object.assign(post, { comments: [] });
		Object.assign(post, {
			comments: post.comments.concat([
				{
					username: req.user.username,
					time: new Date(),
					comment: jsonpack.unpack(req.body).comment
				}
			])
		});

		PostDB.updatePost(post, status => res.json(status));
	});
});

router.post('/posts', (req, res) => {
	PostDB.getPosts(jsonpack.unpack(req.body).users, jsonpack.unpack(req.body).skip, 10, posts => {
		res.json(posts);
	});
});

router.post('/posts-users', (req, res) => {
	PostDB.getPosts(jsonpack.unpack(req.body).users, jsonpack.unpack(req.body).skip, 10, posts => {
		let returnUsers = _users => {
			let users = [];
			for (let i = 0; i < _users.length; i++) {
				let user = _users[i]._doc;
				if (user._id + '' == req.user._id + '') user = Object.assign({}, user, { me: true });
				if (user.followers.map(u => u + '').indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollowing: true });
				if (user.following.map(u => u + '').indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollower: true });
				users.push(user);
			}
			res.json(jsonpack.pack({ posts, users }));
		};

		if (jsonpack.unpack(req.body).query) UserDB.getUsersByQuery(jsonpack.unpack(req.body).query, returnUsers);
		else UserDB.getUsersById([...new Set(jsonpack.unpack(req.body).usersP.concat(posts.map(p => p.user)))], returnUsers);
	});
});
