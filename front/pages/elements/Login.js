import React from 'react';

export default class Login extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			username: {
				hasError: false
			},
			password: {
				hasError: false
			}
		};

		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(event) {
		let { name, value } = event.target;

		if (name == 'username') {
			this.setState({
				username: {
					hasError: !/^[A-Za-z0-9]*$/.test(value)
				}
			});
		}
		if (name == 'password') {
			this.setState({
				password: {
					hasError: !/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(value)
				}
			});
		}
	}
	render() {
		let { username, password } = this.state;
		return (
			<div className="card">
				<div className="card-block">
					<h4 className="card-title">Login</h4>
					<form action="/login" method="post">
						<fieldset className={'form-group' + (username.hasError ? ' has-danger' : '')}>
							<label htmlFor="usernameInput" className="bmd-label-floating">
								Username
							</label>

							<input
								type="text"
								name="username"
								className={'form-control' + (username.hasError ? ' form-control-danger' : '')}
								id="usernameInput"
								onChange={this.handleChange}
							/>

							{username.hasError && <div className="form-control-feedback">Use only alphanumeric characters.</div>}
							<small className="bmd-help">Use only alphanumeric characters.</small>
						</fieldset>

						<fieldset className={'form-group' + (password.hasError ? ' has-danger' : '')}>
							<label htmlFor="passInput" className="bmd-label-floating">
								Password
							</label>

							<input
								type="password"
								name="password"
								className={'form-control' + (password.hasError ? ' form-control-danger' : '')}
								id="passInput"
								onChange={this.handleChange}
							/>

							{password.hasError && (
								<div className="form-control-feedback">Use only alphanumeric characters. Minimum of 8 characters, one letter and one number.</div>
							)}
							<small className="bmd-help">Use only alphanumeric characters. Minimum of 8 characters, one letter and one number.</small>
						</fieldset>

						<button type="submit" className="btn btn-raised btn-primary">
							Login
						</button>
					</form>
				</div>
			</div>
		);
	}
}
