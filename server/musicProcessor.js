var exec = require('child_process').exec;

export const copyFile = (fromPath, toPath, callback) => {
	exec('copy "' + fromPath + '" "' + toPath + '"', (err, stdout, stderr) => {
		callback(err);
	});
};

export const compile = (userId, track, callback) => {
	let command = '';
	let rows = [];

	let mappedSound = [];
	for (let i = 0; i < track.bars.length; i++) {
		for (let j = 0; j < track.bars[i].notes.length; j++) {
			if (track.bars[i].notes[j].enabled) mappedSound.push([i, j]);
		}
	}

	if (mappedSound.length == 0) {
		command =
			'sox -n -r 16k ' +
			'".\\public\\instruments\\custom\\' +
			userId +
			'\\' +
			track.id +
			'.wav" trim 0 ' +
			(track.bars[0].notes.length * (15.0 / parseFloat(track.properties.bpm))).toString();
	} else {
		let lastI = -1;
		for (let i = 0; i < mappedSound.length; i++) {
			let note = track.bars[mappedSound[i][0]].notes[mappedSound[i][1]];
			let oneSilence = 15.0 / parseFloat(track.properties.bpm);
			note.duration = parseFloat(note.properties.duration) * 15.0 / parseFloat(track.properties.bpm);

			if (mappedSound[i][0] != lastI) {
				if (command != '') command += rows[rows.length - 1].replace('_row', '_whole_row');

				let url = '.\\public\\' + track.urls[mappedSound[i][0]].replace(/\//g, '\\').replace(/\.wav$/, '');
				rows.push(
					'".\\public\\processing_' +
						new Date().getTime() +
						'_for_' +
						userId +
						'\\' +
						track.id +
						'\\' +
						mappedSound[i][0] +
						'_' +
						url.split('\\')[url.split('\\').length - 1].replace(/\.wav$/, '') +
						'_' +
						i +
						'_row.wav"'
				);

				if (command != '') command += ' && ';
				command +=
					'sox -S -v ' +
					track.properties.volume +
					' "' +
					url +
					'.wav" ' +
					rows[rows.length - 1] +
					' rate 16000 pad 0 4 pitch ' +
					(note.properties.pitch * 100).toString();

				command += ' && sox -S ';
			}

			command += '"|sox ' + rows[rows.length - 1].replace(/\"/g, '\\"') + ' -p trim 0 ' + note.duration.toString();
			if (mappedSound[i][0] != lastI) {
				let xxyy = (mappedSound[i][1] * oneSilence).toString();
				if (xxyy.indexOf('e') != -1) {
					//console.log(xxyy);
					xxyy=0;
				}
				command += ' pad ' + xxyy + ' 0';
			} else {
				let xxyy = ((mappedSound[i][1] - mappedSound[i - 1][1]) * oneSilence - track.bars[mappedSound[i - 1][0]].notes[mappedSound[i - 1][1]].duration).toString();
				if (xxyy.indexOf('e') != -1) {
					//console.log(xxyy);
					xxyy=0;
				}
				command += ' pad ' + xxyy + ' 0';
			}
			command += '" ';

			lastI = mappedSound[i][0];
		}
		command += rows[rows.length - 1].replace('_row', '_whole_row');
		command +=
			' && sox -S ' +
			(rows.length > 1 ? '-m ' : '') +
			rows.map(r => '-v 1 ' + r.replace('_row', '_whole_row')).join(' ') +
			' ".\\public\\instruments\\custom\\' +
			userId +
			'\\' +
			track.id +
			'.wav"';

		let { gain, tremoloSpeed, tremoloDepth, treble, bass, fadeInType, fadeIn, fadeOutType, fadeOutStart, fadeOut } = track.properties;
		if (gain && gain > 0) command += ' gain ' + gain;
		if (tremoloSpeed && tremoloSpeed > 0 && tremoloDepth && tremoloDepth > 0) command += ' tremolo ' + tremoloSpeed + ' ' + tremoloDepth;
		if (treble && treble != 0) command += ' treble ' + treble;
		if (bass && bass != 0) command += ' bass ' + bass;
		if (fadeInType && fadeIn && fadeIn > 0) command += ' fade ' + fadeInType + ' ' + fadeIn;
		if (fadeOutType && fadeOutStart && fadeOutStart > 0 && fadeOut && fadeOut > 0) command += ' fade ' + fadeOutType + ' 0 ' + fadeOutStart + ' ' + fadeOut;
	}

	if (rows.length > 0) {
		let allFolders = rows.map(u => u.substring(0, u.length - u.split('\\')[u.split('\\').length - 1].length - 1)).map(f => '"' + f.replace(/\"/g, '') + '"');
		allFolders = [...new Set(allFolders)].filter(f => f != '' && f != ' ');

		let del =
			'rd /s /q ' +
			allFolders
				.map(f => f.substring(0, f.length - f.split('\\')[3].length - 1) + '"')
				.filter(f => f.includes('processing_'))
				.join(' && rd /s /q ');

		command = allFolders.map(f => 'mkdir ' + f).join(' && ') + ' && ' + command + ' && ' + del + ' && ' + del + ' && ' + del.split(' && ')[del.split(' && ').length - 1];
	}

	command += ' && echo Done!';

	let cmds = command.split(' && ');
	let cmd = i => {
		try {
			//console.log(cmds[i]);
			if (i + 1 < cmds.length) {
				exec(cmds[i], (err, stdout, stderr) => {
					//if (stderr != 'The system cannot find the file specified.\r\n') console.log(err, stdout, stderr);
					cmd(i + 1);
				});
			} else {
				exec(cmds[i], (err, stdout, stderr) => {
					//if (stderr != 'The system cannot find the file specified.\r\n') console.log(err, stdout, stderr);
					callback(stderr);
				});
			}
		} catch (err) {
			//console.log(err);
			callback(true);
		}
	};
	cmd(0);
};
