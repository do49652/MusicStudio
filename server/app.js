import express from 'express';
import passport from 'passport';
import bodyParser from 'body-parser';
import session from 'express-session';

import login from './routes/login';
import profile from './routes/profile';
import posts from './routes/posts';
import music from './routes/music';

var app = express();
app.use(express.static('public'));
app.set('views', __dirname + '/../views');
app.set('view engine', 'pug');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.text());
app.use(
	session({
		secret: 'secret',
		saveUninitialized: true,
		resave: true
	})
);
app.use(passport.initialize());
app.use(passport.session());

/*
app.get('*.js', (req, res, next) => {
	req.url = req.url + '.gz';
	res.set('Content-Encoding', 'gzip');
	next();
});*/

let auth = (req, res, next) => {
	if (req.isAuthenticated()) return next();
	res.redirect('/login');
};
let render = (req, res) => res.render('index', { title: 'Music Studio' });

app.use(login);
app.use(profile);
app.use(posts);
app.use(music);
app.get('*', auth, render);

app.listen(process.env.PORT || 3000, console.log('\n\nServer is ready...'));
