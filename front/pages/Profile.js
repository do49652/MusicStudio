import React from 'react';
import * as jsonpack from 'jsonpack';

import Header from './elements/Header';
import { NewPost, Post, PostContainer } from './elements/Post';

const headers = {
	Accept: 'application/json',
	'Content-Type': 'text/plain'
};

export default class Profile extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			user: null
		};
	}
	componentDidMount() {
		let body = jsonpack.pack({ user: this.props.id });
		fetch(new Request(this.props.id ? '/profile' : '/me', { method: 'post', headers, credentials: 'same-origin', body }))
			.then(res => res.text())
			.then(res => this.setState({ user: jsonpack.unpack(res) }));
	}
	render() {
		return (
			<div>
				<Header title={'Music Studio'} currentPage={this.props.id ? 'SomeProfile' : 'Profile'} />
				<div className="container">
					{!this.props.id && this.state.user && <ProfileSettingsHeader user={this.state.user} />}
					<br />
					{this.state.user && (this.props.id ? <ProfileWideCard user={this.state.user} /> : <NewPost className="card bg-light border border-primary" user={this.state.user} />)}
					<div className="container">{this.state.user && <PostContainer users={[this.state.user._id]} />}</div>
				</div>
			</div>
		);
	}
}

export class ProfileWideCard extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = { following: false };

		this.follow = this.follow.bind(this);
	}
	follow() {
		this.setState({ following: true });

		let body = jsonpack.pack({
			user: this.props.user._id
		});

		fetch(new Request(this.props.user.meFollowing ? '/unfollow' : '/follow', { method: 'post', headers, credentials: 'same-origin', body }))
			.then(res => res.text())
			.then(res => window.location.reload());
	}
	render() {
		return (
			<div className="card bg-primary">
				<div className="card-body row">
					<div className="col-xs-4">
						<img className="img-fluid" src={this.props.user.profilePicture || '/img/profile.jpg'} />
						<div className="hidden-md-up">
							<br />
						</div>
					</div>
					<div className="col-xs-8">
						<h3 className="card-title">
							{this.props.user.username + ' '}
							<small>
								{!this.props.user.me && (
									<button className={'btn btn-outline-light my-2 my-sm-0' + (this.state.following ? ' disabled' : '')} onClick={this.follow}>
										{(this.props.user.meFollowing ? 'Unfollow' : 'Follow') + (this.state.following ? 'ing...' : '')}
									</button>
								)}
							</small>
						</h3>
						<div className="card-text">
							<a className="text-light" href={'/profile/' + this.props.user._id + '/followers'}>
								<span className="badge badge-dark">{this.props.user.followers.length}</span> Followers
							</a>
							<br />
							<a className="text-light" href={'/profile/' + this.props.user._id + '/following'}>
								<span className="badge badge-dark">{this.props.user.following.length}</span> Following
							</a>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export class ProfileCard extends React.Component {
	constructor(...args) {
		super(...args);
	}
	render() {
		return (
			<div className="card bg-light col-lg-2 col-md-3 col-sm-6 col-xs-12">
				<div className="card-body text-center">
					<h3 className="card-title">
						<a href={'/profile/' + this.props.user._id}>{this.props.user.username}</a>
					</h3>
					<img className="mx-auto d-block" src={this.props.user.profilePicture || '/img/profile.jpg'} />
				</div>
			</div>
		);
	}
}

export class ProfileSettingsHeader extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			user: this.props.user,
			profilePicture: this.props.user.profilePicture,
			uploading: false
		};

		this.resize = this.resize.bind(this);
		this.upload = this.upload.bind(this);
	}
	upload() {
		this.setState({ uploading: true });

		let body = jsonpack.pack({
			profilePicture: this.state.profilePicture
		});

		fetch(new Request('/edit-profile', { method: 'post', headers, credentials: 'same-origin', body }))
			.then(res => res.text())
			.then(res => window.location.reload());
	}
	resize(e) {
		const MAX_WIDTH = 100;
		const MAX_HEIGHT = 100;

		let reader = new FileReader();
		reader.readAsDataURL(e.target.files[0]);

		reader.onload = e => {
			let img = document.createElement('img');
			img.src = e.target.result;

			img.onload = () => {
				let canvas = document.createElement('canvas');

				if (img.width > MAX_WIDTH || img.height > MAX_HEIGHT) {
					canvas.width = MAX_WIDTH;
					canvas.height = MAX_HEIGHT;

					if (img.width > img.height) canvas.height = img.height * MAX_WIDTH / img.width;
					else canvas.width = img.width * MAX_HEIGHT / img.height;
				}

				canvas.getContext('2d').drawImage(img, 0, 0, canvas.width, canvas.height);
				this.setState({ profilePicture: canvas.toDataURL('image/png') });
			};
		};
	}
	render() {
		let { title } = this.state;
		return (
			<div className={'bg-light border border-primary text-center'}>
				<a className="btn btn-primary" data-toggle="collapse" href="#profileSettings">
					<i className="material-icons">settings</i>
				</a>
				<div className="collapse" id="profileSettings">
					<div className="container text-left">
						<div className="row">
							<div className="col-xs-2">
								<img className="img-fluid border border-secondary" src={this.state.profilePicture || '/img/profile.jpg'} />
							</div>
							<div className="col-xs-10">
								<label>
									<b>Profile picture</b>
								</label>
								<div className="input-group">
									<input className="form-control" type="file" accept="image/png, image/jpeg, image/gif" onChange={this.resize} />
								</div>
								<div className="text-right">
									<button onClick={this.upload} className={'btn btn-raised btn-primary my-2 my-sm-0' + (this.state.uploading ? ' disabled' : '')}>
										{this.state.uploading ? 'Uploading...' : 'Upload'}
									</button>
								</div>
							</div>
						</div>
					</div>
					<br />
				</div>
			</div>
		);
	}
}
