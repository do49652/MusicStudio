import mongoose from 'mongoose';
import { Song } from './models';
import { mongodb } from '../properties';

export const getSong = (id, callback) => {
	mongoose.connect(mongodb, () => {
		Song.find({ _id: id }, (err, songs) => {
			if (songs.length == 0) return callback(null);
			callback(songs[0]);
		});
	});
};

export const getSongs = (users, callback) => {
	mongoose.connect(mongodb, () => {
		Song.find({ user: { $in: users } })
			.sort({ creationDate: -1 })
			.exec((err, songs) => callback(songs));
	});
};

export const getSongsById = (ids, callback) => {
	mongoose.connect(mongodb, () => {
		Song.find({ _id: { $in: ids } })
			.sort({ creationDate: -1 })
			.exec((err, songs) => callback(songs));
	});
};

export const addSong = (user, name, song, callback) => {
	mongoose.connect(mongodb, () => {
		let music = new Song({ name, user, song });
		music.save((err, product) => {
			if (err) console.log(err);
			callback(err || product._id);
		});
	});
};

export const deleteSong = (id, callback) => {
	mongoose.connect(mongodb, () => {
		Song.remove({ _id: id }).exec(err => callback(err));
	});
};

export const saveSong = (user, _song, callback) => {
	mongoose.connect(mongodb, () => {
		Song.find({ user }, (err, songs) => {
			if (songs.length == 0) return callback(false);
			songs = songs.filter(s => s.song.track.id + '' == _song.song.id + '');
			if (songs.length == 0) return callback(false);

			let song = songs[0];
			Object.assign(song, { song: { bpm: song.song.bpm, bars: song.song.bars, beats: song.song.beats, sections: _song.sections, track: _song.song } });

			song.save((err, product) => {
				if (err) return console.log(err);
				callback(true);
			});
		});
	});
};
