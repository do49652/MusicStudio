import React from 'react';
import * as jsonpack from 'jsonpack';

import Header from './elements/Header';
import { ProfileCard } from './Profile';

const headers = {
	Accept: 'application/json',
	'Content-Type': 'text/plain'
};

export default class ProfileList extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			users: null
		};
	}
	componentDidMount() {
		if (this.props.query) {
			let body = jsonpack.pack({ query: this.props.query });
			fetch(new Request('/profiles', { method: 'post', headers, credentials: 'same-origin', body }))
				.then(res => res.text())
				.then(res => this.setState({ users: jsonpack.unpack(res) }));
		}

		if (this.props.id) {
			let body = jsonpack.pack({ user: this.props.id });
			fetch(new Request('/profile', { method: 'post', headers, credentials: 'same-origin', body }))
				.then(user => user.text())
				.then(user => {
					body = jsonpack.pack({ users: this.props.page == 'Followers' ? jsonpack.unpack(user).followers : jsonpack.unpack(user).following });
					fetch(new Request('/profiles', { method: 'post', headers, credentials: 'same-origin', body }))
						.then(res => res.text())
						.then(res => this.setState({ users: jsonpack.unpack(res) }));
				});
		}
	}
	render() {
		return (
			<div>
				<Header title={'Music Studio'} currentPage={this.props.page} query={this.props.query} />
				<br />
				<div className="container">
					{this.props.page && <h4>{this.props.page}</h4>}
					{!this.state.users && <h4>Loading...</h4>}
					{this.state.users && this.state.users.length == 0 && <h4>No results.</h4>}
					{this.state.users &&
						this.state.users.length > 0 &&
						this.props.query && (
							<h4>
								Found {this.state.users.length} results for <i>{this.props.query}</i>
							</h4>
						)}
					<div className="row">{this.state.users && this.state.users.map(u => <ProfileCard user={u} key={u._id} />)}</div>
				</div>
			</div>
		);
	}
}
