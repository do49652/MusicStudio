import React from 'react';
import { Link, Redirect } from 'react-router-dom';

export default class Header extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			query: this.props.query
		};

		this.handleChange = this.handleChange.bind(this);
		this.handleSumbit = this.handleSumbit.bind(this);
	}
	handleSumbit(e) {
		e.preventDefault();
		document.location.href = '/search/' + this.state.query;
	}
	handleChange(e) {
		this.setState({ query: e.target.value });
	}
	render() {
		let { title, currentPage } = this.props;
		let pages = [{ title: 'Home', link: '/' }, { title: 'Profile', link: '/profile' }, { title: 'Editor', link: '/editor' }];
		if (this.props.className) pages = [];

		return (
			<nav className={'navbar navbar-expand-lg navbar-dark bg-primary'}>
				<span className="navbar-brand">{title}</span>

				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
					<span className="navbar-toggler-icon" />
				</button>

				<div className="collapse navbar-collapse" id="navbar">
					<ul className="navbar-nav mr-auto mt-2 mt-lg-0">
						{pages.map(p => (
							<li className={'nav-item' + (p.title == currentPage ? ' active' : '')} key={pages.indexOf(p)}>
								<Link className="nav-link" to={p.link}>
									{p.title}
								</Link>
							</li>
						))}
					</ul>

					<ul className="navbar-nav mr-auto mt-2 mt-lg-0">
						<form className="form-inline" onSubmit={this.handleSumbit}>
							<span className="bmd-form-group">
								<input onChange={this.handleChange} className="form-control mr-sm-2" type="search" placeholder="Search" />
							</span>

							<button className="btn btn-outline-light my-2 my-sm-0" onClick={this.handleSumbit}>
								Search
							</button>
						</form>
					</ul>

					<a className="btn btn-raised btn-danger my-2 my-sm-0" href="/logout">
						Logout
					</a>
				</div>
			</nav>
		);
	}
}
