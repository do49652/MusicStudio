'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.Song = exports.Post = exports.User = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var arrayOfOids = { type: [_mongoose.Schema.Types.ObjectId], default: [] };

var userSchema = new _mongoose.Schema({
	username: String,
	email: String,
	password: String,
	profilePicture: { type: String, default: null },
	creationDate: { type: Date, default: Date.now },
	songs: arrayOfOids,
	posts: arrayOfOids,
	likedPosts: arrayOfOids,
	likedComments: arrayOfOids,
	following: arrayOfOids,
	followers: arrayOfOids
});
var User = exports.User = _mongoose2.default.model('User', userSchema);

var postSchema = new _mongoose.Schema({
	user: _mongoose.Schema.Types.ObjectId,
	text: String,
	creationDate: { type: Date, default: Date.now },
	song: { type: String, default: null },
	comments: _mongoose.Schema.Types.Mixed,
	usersLikes: arrayOfOids
});
var Post = exports.Post = _mongoose2.default.model('Post', postSchema);

var songSchema = new _mongoose.Schema({
	name: String,
	user: _mongoose.Schema.Types.ObjectId,
	creationDate: { type: Date, default: Date.now },
	song: _mongoose.Schema.Types.Mixed
});
var Song = exports.Song = _mongoose2.default.model('Song', songSchema);