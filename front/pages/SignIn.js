import React from 'react';
import Login from './elements/Login';
import Register from './elements/Register';

export default class SignIn extends React.Component {
	constructor(...args) {
		super(...args);
	}

	render() {
		return (
			<div className="container h-100">
				<div className="row justify-content-center align-items-center h-100">
					<div className="col-md-6">
						<Login />
					</div>
					<div className="col-md-6">
						<Register />
					</div>
				</div>
			</div>
		);
	}
}
