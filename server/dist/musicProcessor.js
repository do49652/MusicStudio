'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var exec = require('child_process').exec;

var copyFile = exports.copyFile = function copyFile(fromPath, toPath, callback) {
	exec('copy "' + fromPath + '" "' + toPath + '"', function (err, stdout, stderr) {
		callback(err);
	});
};

var compile = exports.compile = function compile(userId, track, callback) {
	var command = '';
	var rows = [];

	var mappedSound = [];
	for (var i = 0; i < track.bars.length; i++) {
		for (var j = 0; j < track.bars[i].notes.length; j++) {
			if (track.bars[i].notes[j].enabled) mappedSound.push([i, j]);
		}
	}

	if (mappedSound.length == 0) {
		command = 'sox -n -r 16k ' + '".\\public\\instruments\\custom\\' + userId + '\\' + track.id + '.wav" trim 0 ' + (track.bars[0].notes.length * (15.0 / parseFloat(track.properties.bpm))).toString();
	} else {
		var lastI = -1;
		for (var _i = 0; _i < mappedSound.length; _i++) {
			var note = track.bars[mappedSound[_i][0]].notes[mappedSound[_i][1]];
			var oneSilence = 15.0 / parseFloat(track.properties.bpm);
			note.duration = parseFloat(note.properties.duration) * 15.0 / parseFloat(track.properties.bpm);

			if (mappedSound[_i][0] != lastI) {
				if (command != '') command += rows[rows.length - 1].replace('_row', '_whole_row');

				var url = '.\\public\\' + track.urls[mappedSound[_i][0]].replace(/\//g, '\\').replace(/\.wav$/, '');
				rows.push('".\\public\\processing_' + new Date().getTime() + '_for_' + userId + '\\' + track.id + '\\' + mappedSound[_i][0] + '_' + url.split('\\')[url.split('\\').length - 1].replace(/\.wav$/, '') + '_' + _i + '_row.wav"');

				if (command != '') command += ' && ';
				command += 'sox -S -v ' + track.properties.volume + ' "' + url + '.wav" ' + rows[rows.length - 1] + ' rate 16000 pad 0 4 pitch ' + (note.properties.pitch * 100).toString();

				command += ' && sox -S ';
			}

			command += '"|sox ' + rows[rows.length - 1].replace(/\"/g, '\\"') + ' -p trim 0 ' + note.duration.toString();
			if (mappedSound[_i][0] != lastI) {
				var xxyy = (mappedSound[_i][1] * oneSilence).toString();
				if (xxyy.indexOf('e') != -1) {
					//console.log(xxyy);
					xxyy = 0;
				}
				command += ' pad ' + xxyy + ' 0';
			} else {
				var _xxyy = ((mappedSound[_i][1] - mappedSound[_i - 1][1]) * oneSilence - track.bars[mappedSound[_i - 1][0]].notes[mappedSound[_i - 1][1]].duration).toString();
				if (_xxyy.indexOf('e') != -1) {
					//console.log(xxyy);
					_xxyy = 0;
				}
				command += ' pad ' + _xxyy + ' 0';
			}
			command += '" ';

			lastI = mappedSound[_i][0];
		}
		command += rows[rows.length - 1].replace('_row', '_whole_row');
		command += ' && sox -S ' + (rows.length > 1 ? '-m ' : '') + rows.map(function (r) {
			return '-v 1 ' + r.replace('_row', '_whole_row');
		}).join(' ') + ' ".\\public\\instruments\\custom\\' + userId + '\\' + track.id + '.wav"';

		var _track$properties = track.properties,
		    gain = _track$properties.gain,
		    tremoloSpeed = _track$properties.tremoloSpeed,
		    tremoloDepth = _track$properties.tremoloDepth,
		    treble = _track$properties.treble,
		    bass = _track$properties.bass,
		    fadeInType = _track$properties.fadeInType,
		    fadeIn = _track$properties.fadeIn,
		    fadeOutType = _track$properties.fadeOutType,
		    fadeOutStart = _track$properties.fadeOutStart,
		    fadeOut = _track$properties.fadeOut;

		if (gain && gain > 0) command += ' gain ' + gain;
		if (tremoloSpeed && tremoloSpeed > 0 && tremoloDepth && tremoloDepth > 0) command += ' tremolo ' + tremoloSpeed + ' ' + tremoloDepth;
		if (treble && treble != 0) command += ' treble ' + treble;
		if (bass && bass != 0) command += ' bass ' + bass;
		if (fadeInType && fadeIn && fadeIn > 0) command += ' fade ' + fadeInType + ' ' + fadeIn;
		if (fadeOutType && fadeOutStart && fadeOutStart > 0 && fadeOut && fadeOut > 0) command += ' fade ' + fadeOutType + ' 0 ' + fadeOutStart + ' ' + fadeOut;
	}

	if (rows.length > 0) {
		var allFolders = rows.map(function (u) {
			return u.substring(0, u.length - u.split('\\')[u.split('\\').length - 1].length - 1);
		}).map(function (f) {
			return '"' + f.replace(/\"/g, '') + '"';
		});
		allFolders = [].concat(_toConsumableArray(new Set(allFolders))).filter(function (f) {
			return f != '' && f != ' ';
		});

		var del = 'rd /s /q ' + allFolders.map(function (f) {
			return f.substring(0, f.length - f.split('\\')[3].length - 1) + '"';
		}).filter(function (f) {
			return f.includes('processing_');
		}).join(' && rd /s /q ');

		command = allFolders.map(function (f) {
			return 'mkdir ' + f;
		}).join(' && ') + ' && ' + command + ' && ' + del + ' && ' + del + ' && ' + del.split(' && ')[del.split(' && ').length - 1];
	}

	command += ' && echo Done!';

	var cmds = command.split(' && ');
	var cmd = function cmd(i) {
		try {
			//console.log(cmds[i]);
			if (i + 1 < cmds.length) {
				exec(cmds[i], function (err, stdout, stderr) {
					//if (stderr != 'The system cannot find the file specified.\r\n') console.log(err, stdout, stderr);
					cmd(i + 1);
				});
			} else {
				exec(cmds[i], function (err, stdout, stderr) {
					//if (stderr != 'The system cannot find the file specified.\r\n') console.log(err, stdout, stderr);
					callback(stderr);
				});
			}
		} catch (err) {
			//console.log(err);
			callback(true);
		}
	};
	cmd(0);
};