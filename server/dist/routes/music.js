'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _jsonpack = require('jsonpack');

var jsonpack = _interopRequireWildcard(_jsonpack);

var _musicDB = require('./musicDB');

var MusicDB = _interopRequireWildcard(_musicDB);

var _musicProcessor = require('../musicProcessor');

var MusicProcessor = _interopRequireWildcard(_musicProcessor);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
exports.default = router;


var createFolder = function createFolder(req, res, next) {
	if (!req.user) return null;
	var dir = __dirname + '/../../public/instruments/custom/' + req.user._id + '/';
	if (!_fs2.default.existsSync(dir)) _fs2.default.mkdirSync(dir);
	return next();
};

router.post('/new-song', function (req, res) {
	return MusicDB.addSong(req.user._id, jsonpack.unpack(req.body).name, jsonpack.unpack(req.body).song, function (id) {
		return res.json(id);
	});
});
router.post('/songs', function (req, res) {
	return MusicDB.getSongs(jsonpack.unpack(req.body).users || [req.user._id], function (songs) {
		return res.json(jsonpack.pack(songs));
	});
});
router.post('/get-song', function (req, res) {
	return MusicDB.getSong(jsonpack.unpack(req.body).id, function (song) {
		return res.json(jsonpack.pack(song));
	});
});
router.post('/save-song', createFolder, function (req, res) {
	return MusicDB.saveSong(req.user._id, { song: jsonpack.unpack(req.body).song, sections: jsonpack.unpack(req.body).sections }, function (status) {
		MusicProcessor.compile(req.user._id, jsonpack.unpack(req.body).wav, function () {
			return res.json(status);
		});
	});
});
router.post('/delete-song', function (req, res) {
	return MusicDB.deleteSong(jsonpack.unpack(req.body).id, function (status) {
		return res.json(status);
	});
});