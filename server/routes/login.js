import express from 'express';
import crypto from 'crypto';
import passport from 'passport';
import { Strategy } from 'passport-local';

import * as UserDB from './userDB';

const router = express.Router();
export default router;

const config = { usernameField: 'username', passwordField: 'password', passReqToCallback: true };
passport.serializeUser((user, done) => done(null, user._id));
passport.deserializeUser((id, done) => UserDB.getUserById(id, user => done(null, user)));
passport.use(
	'local-login',
	new Strategy(config, (req, username, password, done) => {
		password = crypto
			.createHash('sha256')
			.update(password)
			.digest('base64');

		UserDB.getUser(username, password, user => done(null, user));
	})
);

const authenticate = passport.authenticate('local-login', { successRedirect: '/', failureRedirect: '/login' });

router.post('/register', (req, res) => {
	let { email, username, password } = req.body;
	password = crypto
		.createHash('sha256')
		.update(password)
		.digest('base64');

	UserDB.addUser(email, username, password, () => authenticate(req, res));
});

router.get('/login', (req, res) => res.render('login'));
router.post('/login', authenticate);
router.get('/logout', (req, res) => {
	req.logout();
	res.redirect('/login');
});
