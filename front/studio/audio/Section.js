import { LiveTrack } from './Track';
import Buffer from './utils/Buffer';
import Instruments from './Instuments';

export class Note {
	constructor(bar, properties) {
		this.bar = bar;
		this.properties = properties;
		this.sound = null;
		this.enabled = false;
		this.step = null;
	}
}

export class Bar {
	constructor(section, name, sound, properties) {
		this.section = section;
		this.name = name;
		this.sound = sound;
		this.properties = properties;

		this.notes = [];
		for (let i = 0; i < section.columns; i++) this.notes.push(new Note(this, Object.assign({}, properties)));
	}
}

export default class Section {
	constructor(name, instrument, properties, columns, id) {
		this.name = columns ? name : name.name;
		this.instrument = columns ? instrument : name.instrument;
		this.properties = columns ? properties : name.properties;
		this.columns = columns || name.columns;
		this.id = columns ? id : name.id;
		this.section = !columns ? name : null;

		this.tracks = [];
		this.liveTrack = null;

		this.currentPosition = 0;
		this.indicator = null;
		this.playing = false;

		this.bars = [];

		if (this.instrument == null) {
			if (this.section && this.section.sounds && this.section.sounds.length > 0) {
				for (let i = 0; i < this.section.sounds.length; i++) {
					let { sound, name, properties } = this.section.sounds[i];
					this.bars.push(new Bar(this, name, sound, properties));
				}
			}
		} else if (this.instrument.sounds) {
			for (let i = 0; i < this.instrument.sounds.length; i++) {
				let url = 'instruments/' + this.instrument.dir + '/' + this.instrument.sounds[i] + '.wav';
				this.bars.push(new Bar(this, this.instrument.sounds[i], url, this.properties));
			}
		} else if (this.instrument.note) {
			let url = 'instruments/' + this.instrument.dir + '/' + this.instrument.note + '.wav';
			for (let i = this.instrument.span.max; i >= this.instrument.span.min; i--)
				this.bars.push(new Bar(this, Instruments.GetNoteName(this.instrument.noteName, i), url, Object.assign({}, this.properties, { pitch: i })));
		}
	}
	newSound(soundName, fileDir, fileName, properties, sectionEditor, callback) {
		if (this.instrument && this.instrument.note) return;

		let url = 'instruments/' + fileDir + '/' + fileName + '.wav';
		this.bars.push(new Bar(this, soundName, url, Object.assign({}, this.properties, properties)));

		if (this.liveTrack.buffer == null) {
			this.liveTrack.buffer = new Buffer(window.audioContext, [url]);
			this.liveTrack.buffer.loadSound(0, () =>
				this.liveTrack.load(0, 0, () => {
					sectionEditor.setState(sectionEditor.set(this), () => {
						sectionEditor.resize();
					});
					if (callback) callback();
				})
			);
		} else {
			this.liveTrack.buffer.urls.push(url);
			this.liveTrack.buffer.loadSound(this.liveTrack.buffer.urls.length - 1, () => {
				this.liveTrack.load(this.bars.length - 1, 0, () => {
					sectionEditor.setState(sectionEditor.set(this), () => {
						sectionEditor.resize();
					});
					if (callback) callback();
				});
			});
		}
	}
	loadTrack(callback) {
		this.liveTrack = new LiveTrack(this, callback);
	}
	setNote(row, column, enabled) {
		this.bars[row].notes[column].enabled = enabled;
	}
	getNote(row, column) {
		return this.bars[row].notes[column];
	}
	play(f) {
		if (!this.indicator) return;
		if (!f) this.playing = true;

		let _play = () => {
			this.currentPosition = this.indicator.getPosition();
			this.liveTrack.playPosition(this.currentPosition);

			this.timer = setTimeout(() => {
				if (!this.playing) return;
				this.indicator.increment();
				this.play(true);
			}, 60000 / 4 / this.properties.bpm);
		};

		if (!f) this.liveTrack = new LiveTrack(this, _play);
		else _play();
	}
	stop() {
		if (!this.indicator) return;
		this.playing = false;
		this.indicator.setPosition(0);
		try {
			this.liveTrack.stop();
		} catch (err) {}
		clearTimeout(this.timer);
	}
	save() {
		let section = {
			name: this.name || 'Name',
			instrument: this.instrument,
			properties: this.properties,
			columns: this.columns,
			sounds: this.bars.map(b => ({ name: b.name, sound: b.sound, properties: b.properties })),
			notes: [],
			id: this.id
		};
		for (let i = 0; i < this.bars.length; i++) {
			let bar = [];
			for (let j = 0; j < this.bars[i].notes.length; j++) {
				if (this.bars[i].notes[j].enabled) {
					let { properties } = this.bars[i].notes[j];
					section.notes.push({ i, j, properties });
				}
			}
		}

		return section;
	}
	load(callback) {
		let loader = () => {
			if (!this.section) return;
			try {
				for (let i = 0; i < this.section.notes.length; i++) {
					let note = this.section.notes[i];
					this.bars[note.i].notes[note.j].step.mouseClick(true, false, note.properties.duration);
					this.bars[note.i].notes[note.j].properties = note.properties;
				}
				this.section = null;
				callback();
			} catch (err) {
				setTimeout(loader, 100);
			}
		};

		loader();
	}
}
