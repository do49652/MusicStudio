'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _passportLocal = require('passport-local');

var _userDB = require('./userDB');

var UserDB = _interopRequireWildcard(_userDB);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
exports.default = router;


var config = { usernameField: 'username', passwordField: 'password', passReqToCallback: true };
_passport2.default.serializeUser(function (user, done) {
	return done(null, user._id);
});
_passport2.default.deserializeUser(function (id, done) {
	return UserDB.getUserById(id, function (user) {
		return done(null, user);
	});
});
_passport2.default.use('local-login', new _passportLocal.Strategy(config, function (req, username, password, done) {
	password = _crypto2.default.createHash('sha256').update(password).digest('base64');

	UserDB.getUser(username, password, function (user) {
		return done(null, user);
	});
}));

var authenticate = _passport2.default.authenticate('local-login', { successRedirect: '/', failureRedirect: '/login' });

router.post('/register', function (req, res) {
	var _req$body = req.body,
	    email = _req$body.email,
	    username = _req$body.username,
	    password = _req$body.password;

	password = _crypto2.default.createHash('sha256').update(password).digest('base64');

	UserDB.addUser(email, username, password, function () {
		return authenticate(req, res);
	});
});

router.get('/login', function (req, res) {
	return res.render('login');
});
router.post('/login', authenticate);
router.get('/logout', function (req, res) {
	req.logout();
	res.redirect('/login');
});