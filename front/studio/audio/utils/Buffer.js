export default class Buffer {
	constructor(context, urls) {
		this.context = context;
		this.urls = urls;
		this.buffers = [];
	}

	loadSound(index, callback) {
		let request = new XMLHttpRequest();

		let path = this.urls[index].replace('#', '%23');

		request.open('get', '../' + path, true);
		request.responseType = 'arraybuffer';

		request.onload = () => {
			this.context.decodeAudioData(request.response, buffer => {
				this.buffers[index] = buffer;
				if (index == this.urls.length - 1) callback && callback();
				else this.loadSound(index + 1, callback);
			});
		};
		request.send();
	}

	reloadSound(index, callback) {
		let request = new XMLHttpRequest();

		let path = this.urls[index].replace('#', '%23');

		request.open('get', '../' + path, true);
		request.responseType = 'arraybuffer';

		request.onload = () => {
			this.context.decodeAudioData(request.response, buffer => {
				this.buffers[index] = buffer;
				callback && callback();
			});
		};
		request.send();
	}

	getSound(index) {
		return this.buffers[index];
	}
}

window.audioContext = window.audioContext || new (window.AudioContext || window.webkitAudioContext)();
