'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _musicProcessor = require('../musicProcessor');

var _jsonpack = require('jsonpack');

var jsonpack = _interopRequireWildcard(_jsonpack);

var _postDB = require('./postDB');

var PostDB = _interopRequireWildcard(_postDB);

var _userDB = require('./userDB');

var UserDB = _interopRequireWildcard(_userDB);

var _musicDB = require('./musicDB');

var MusicDB = _interopRequireWildcard(_musicDB);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var router = _express2.default.Router();
exports.default = router;


router.post('/post', function (req, res) {
	if (jsonpack.unpack(req.body).song) {
		MusicDB.getSong(jsonpack.unpack(req.body).song, function (song) {
			song = '/instruments/custom/' + req.user._id + '/' + song.song.track.id;
			var m = new Date().getTime();

			(0, _musicProcessor.copyFile)('.\\public' + song.replace(/\//g, '\\') + '.wav', '.\\public' + song.replace(/\//g, '\\') + m + '.wav', function (err) {
				if (err) return console.log(err);
				PostDB.addPost(req.user._id, jsonpack.unpack(req.body).text, song + m + '.wav', function (status) {
					return res.json(status);
				});
			});
		});
	} else PostDB.addPost(req.user._id, jsonpack.unpack(req.body).text, null, function (status) {
		return res.json(status);
	});
});

router.post('/comment', function (req, res) {
	PostDB.getPost(jsonpack.unpack(req.body).postId, function (post) {
		if (!post) res.json(null);

		if (!post.comments) Object.assign(post, { comments: [] });
		Object.assign(post, {
			comments: post.comments.concat([{
				username: req.user.username,
				time: new Date(),
				comment: jsonpack.unpack(req.body).comment
			}])
		});

		PostDB.updatePost(post, function (status) {
			return res.json(status);
		});
	});
});

router.post('/posts', function (req, res) {
	PostDB.getPosts(jsonpack.unpack(req.body).users, jsonpack.unpack(req.body).skip, 10, function (posts) {
		res.json(posts);
	});
});

router.post('/posts-users', function (req, res) {
	PostDB.getPosts(jsonpack.unpack(req.body).users, jsonpack.unpack(req.body).skip, 10, function (posts) {
		var returnUsers = function returnUsers(_users) {
			var users = [];
			for (var i = 0; i < _users.length; i++) {
				var user = _users[i]._doc;
				if (user._id + '' == req.user._id + '') user = Object.assign({}, user, { me: true });
				if (user.followers.map(function (u) {
					return u + '';
				}).indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollowing: true });
				if (user.following.map(function (u) {
					return u + '';
				}).indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollower: true });
				users.push(user);
			}
			res.json(jsonpack.pack({ posts: posts, users: users }));
		};

		if (jsonpack.unpack(req.body).query) UserDB.getUsersByQuery(jsonpack.unpack(req.body).query, returnUsers);else UserDB.getUsersById([].concat(_toConsumableArray(new Set(jsonpack.unpack(req.body).usersP.concat(posts.map(function (p) {
			return p.user;
		}))))), returnUsers);
	});
});