const Notes = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B'];

const Instruments = {
	GrandPiano: {
		name: 'Grand Piano',
		dir: '.',
		note: 'Grand Piano-C4',
		noteName: 'C4',
		span: { min: -24, max: 24 }
	},
	AltoSax: {
		name: 'Alto Sax',
		dir: '.',
		note: 'Alto Sax-D4',
		noteName: 'D4',
		span: { min: -12, max: 36 }
	},
	Banjo: {
		name: 'Banjo',
		dir: '.',
		note: 'Banjo-F4',
		noteName: 'F4',
		span: { min: -24, max: 24 }
	},
	Brass: {
		name: 'Brass',
		dir: '.',
		note: 'Brass Section-C4',
		noteName: 'C4',
		span: { min: -24, max: 24 }
	},
	CelloAttack: {
		name: 'Cello Attack',
		dir: '.',
		note: 'CelloAttack-D2',
		noteName: 'D2',
		span: { min: -24, max: 24 }
	},
	Cello: {
		name: 'Cello',
		dir: '.',
		note: 'Cello-B2',
		noteName: 'B2',
		span: { min: -24, max: 24 }
	},
	Clarinet: {
		name: 'Clarinet',
		dir: '.',
		note: 'Clarinet-E4',
		noteName: 'E4',
		span: { min: -24, max: 24 }
	},
	CleanGuitar: {
		name: 'Clean Guitar',
		dir: '.',
		note: 'Clean Guitar-G3',
		noteName: 'G3',
		span: { min: -24, max: 24 }
	},
	ConcertChoir: {
		name: 'Concert Choir',
		dir: '.',
		note: 'Concert Choir-F4',
		noteName: 'F4',
		span: { min: -24, max: 24 }
	},
	DistortionGuitar: {
		name: 'Distortion Guitar',
		dir: '.',
		note: 'Distortion Guitar-D3',
		noteName: 'D3',
		span: { min: -24, max: 24 }
	},
	Flute: {
		name: 'Flute',
		dir: '.',
		note: 'Flute-G5',
		noteName: 'G5',
		span: { min: -24, max: 24 }
	},
	JazzGuitar: {
		name: 'Jazz Guitar',
		dir: '.',
		note: 'Jazz Guitar-C4',
		noteName: 'C4',
		span: { min: -24, max: 24 }
	},
	NylonGuitar: {
		name: 'Nylon Guitar',
		dir: '.',
		note: 'Nylon Guitar-C4',
		noteName: 'C4',
		span: { min: -24, max: 24 }
	},
	OverdriveGuitar: {
		name: 'Overdrive Guitar',
		dir: '.',
		note: 'Overdrive Guitar-C4',
		noteName: 'C4',
		span: { min: -24, max: 24 }
	},
	Recorder: {
		name: 'Recorder',
		dir: '.',
		note: 'Recorder-B4',
		noteName: 'B4',
		span: { min: -24, max: 24 }
	},
	SlapBass: {
		name: 'Slap Bass',
		dir: '.',
		note: 'Slap Bass 1-C4',
		noteName: 'C4',
		span: { min: -24, max: 24 }
	},
	SteelGuitar: {
		name: 'Steel Guitar',
		dir: '.',
		note: 'Steel Guitar-D4',
		noteName: 'D4',
		span: { min: -24, max: 24 }
	},
	Violin: {
		name: 'Violin',
		dir: '.',
		note: 'Violin-D#4',
		noteName: 'D#4',
		span: { min: -24, max: 24 }
	},
	Drums: {
		name: 'Drums',
		dir: 'drums',
		sounds: [
			'Agogo',
			'Bongo Rim',
			'Bongo Tone',
			'Brush Snare',
			'Brushed Tom',
			'Cabasa',
			'Conga High Muted',
			'Conga High Open',
			'Cowbell',
			'Crash Cymbal',
			'Dance Snare',
			'Electric Snare',
			'Electric Tom',
			'Electronic Kick',
			'Gated Kick',
			'Hand Clap',
			'Hi-Hat Closed Hard',
			'Hi-Hat Closed Soft',
			'Hi-Hat Open Attack',
			'Hi-Hat Open',
			'Hi-Hat Pedal',
			'Jazz Bass Drum',
			'Jazz Rim Shot',
			'Jazz Snare Hard',
			'Jazz Snare Soft 1',
			'Jazz Snare Soft 2',
			'Orchestral Cymbal',
			'Power Kick',
			'Power Snare 1',
			'Power Snare 2',
			'Ride Bell',
			'Ride Cymbal',
			'Rim Shot',
			'Room Kick',
			'Room Snare',
			'Room Tom 1',
			'Room Tom 2',
			'Shaker',
			'Splash Cymbal',
			'Standard Kick 1',
			'Standard Kick 2',
			'Standard Kick 3',
			'Standard Snare 1',
			'Standard Snare 2',
			'Standard Snare 3',
			'Standard Snare 4',
			'Standard Tom 1',
			'Standard Tom 2',
			'Standard Tom 3',
			'Standard Tom 4',
			'Standard Tom 5',
			'Sticks',
			'Surdo Muted',
			'Surdo Open',
			'Synth Hand Clap',
			'Taiko Drum',
			'Tambourine',
			'Timbale Low',
			'Timbale Rim',
			'Timpani Hard',
			'Timpani Soft',
			'TR-808 Clap',
			'TR-808 Cowbell',
			'TR-808 Cymbal',
			'TR-808 Hat 1',
			'TR-808 Hat 2',
			'TR-808 Kick',
			'TR-808 Snare',
			'TR-909 Kick',
			'TR-909 Scratch Pull',
			'TR-909 Scratch Push',
			'TR-909 Snare 1',
			'TR-909 Snare 2',
			'Tumba Low'
		]
	},
	Sounds: {
		name: 'Sounds',
		dir: 'sounds',
		sounds: [
			'Birds',
			'Bottle Blow',
			'Breath Noise-1',
			'Breath Noise-2',
			'Brush Swirl',
			'Car-Pass',
			'Car-Skid',
			'Car-Start',
			'Chimes',
			'Chinese Cymbal',
			'Dog',
			'Door Creak',
			'Door Slam',
			'Explosion',
			'Fret Slide',
			'Heartbeat',
			'Helicopter',
			'High Seiko SQ50',
			'Orchestra Hit-1',
			'Orchestra Hit-2',
			'Rain',
			'Reverse Cymbal',
			'Scratch Pull',
			'Scratch Push',
			'Shakuhachi',
			'Siren'
		]
	},
	GetNoteName: (noteName, transposeBy) => {
		let octave = parseInt(noteName.replace('#', '').substring(1));
		let pos = Notes.indexOf(noteName.replace('' + octave, ''));
		let newNoteName = Notes[(pos + transposeBy - 1 > 0 ? transposeBy - 1 : Notes.length + (transposeBy - 1) % Notes.length) % Notes.length];
		if (!newNoteName) {
			let t = transposeBy + 240 - 1;
			newNoteName = Notes[(pos + t > 0 ? t : Notes.length + t % Notes.length) % Notes.length];
		}

		for (let i = 0; i < Math.abs(transposeBy); i++) {
			if (transposeBy < 0) {
				pos = (pos == 0 ? Notes.length - 1 : pos - 1) % Notes.length;
				if (pos == Notes.length - 1) octave--;
			} else {
				pos = (pos + 1) % Notes.length;
				if (pos == 0) octave++;
			}
		}
		newNoteName += octave;

		return newNoteName;
	}
};

export default Instruments;
