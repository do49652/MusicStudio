import React from 'react';
import SectionEditor from '../SectionEditor';

const TopBarColor = 'Wheat';

const ButtonColor = 'Azure';
const ButtonColorHover = 'Bisque';
const ButtonColorPress = 'BurlyWood';

const BackgroundColor = 'DimGray';

const StepColor = 'LightGray';
const StepColorAlt = 'PapayaWhip';
const StepEnabledColor = 'LightSkyBlue';
const StepEnabledColorAlt = 'LightSeaGreen';

export class TopBar extends React.Component {
	constructor(...args) {
		super(...args);

		let { positionX, positionY, width, height } = this.props;
		this.state = {
			backgroundColor: TopBarColor,
			positionX,
			positionY,
			width,
			height
		};
	}
	resize(width, height) {
		this.setState({ width, height });
	}
	render() {
		let { positionX: left, positionY: top, width, height, backgroundColor } = this.state;
		let style = Object.assign({}, { left, top, width, height, backgroundColor }, { position: 'absolute' });

		return (
			<div>
				<span className="step" style={style} />
				<span className="innerShadow" style={style} />
			</div>
		);
	}
}

export class BarProperties extends React.Component {
	constructor(...args) {
		super(...args);

		let { positionX, positionY, width, height, properties } = this.props;
		this.state = {
			positionX,
			positionY,
			width,
			height,
			opened: false,
			properties
		};

		this.open = this.open.bind(this);
		this.close = this.close.bind(this);
	}
	open() {
		this.setState({ opened: true });
	}
	close() {
		this.setState({ opened: false });
	}
	render() {
		if (!this.state.opened) return <div />;

		let { positionX: left, positionY: top, width, height } = this.state;
		let style = Object.assign({}, { left, top, width, height }, { position: 'absolute' });

		return <div style={style}>{this.props.children}</div>;
	}
}

export class Background extends React.Component {
	constructor(...args) {
		super(...args);

		let { positionX, positionY, width, height } = this.props;
		this.state = {
			color: BackgroundColor,
			positionX,
			positionY,
			width,
			height
		};
		this.resize = this.resize.bind(this);
		this.reposition = this.reposition.bind(this);
	}
	resize(width, height) {
		this.setState({ width, height });
	}
	reposition(positionX, positionY) {
		this.setState({ positionX, positionY });
	}
	render() {
		let { positionX: left, positionY: top, width, height, color } = this.state;
		let style = Object.assign({}, { left, top, width, height }, { position: 'absolute', backgroundColor: color });

		return (
			<div>
				<span className="bg step" style={style} />
				<span className="bg innerShadow" style={style} />
			</div>
		);
	}
}

export class Button extends React.Component {
	constructor(...args) {
		super(...args);

		let { toggle, text, pressed } = this.props;
		this.state = {
			pressed,
			toggle,
			text
		};

		this.mouseClick = this.mouseClick.bind(this);
	}
	mouseClick(f) {
		if (!this.state.pressed || !this.props.off) this.props.on(f == true);
		else this.props.off(f == true);

		if (this.props.off) {
			this.setState({
				pressed: this.state.toggle ? !this.state.pressed : false
			});
		}
	}
	render() {
		let { positionX: left, positionY: top, width, height } = this.props;
		let { text, pressed } = this.state;
		let style = Object.assign({}, { left, top, width, height }, { position: 'absolute', padding: '0px' });

		return (
			<button className={'btn ' + (pressed ? 'btn-primary btn-raised' : 'btn-outline-primary')} style={style} onMouseDown={this.mouseClick}>
				{text ? text : ''}
			</button>
		);
	}
}

export class Indicator extends React.Component {
	constructor(...args) {
		super(...args);
		this.state = {
			position: this.props.position
		};

		this.setPosition = this.setPosition.bind(this);
		this.increment = this.increment.bind(this);
	}
	setPosition(position) {
		this.setState({ position });
	}
	getPosition() {
		return this.state.position;
	}
	increment() {
		let end = this.state.position + 1 == this.props.maxPosition;

		this.setState({
			position: end ? 0 : this.state.position + 1
		});

		return end;
	}
	render() {
		let { positionX: left, positionY: top, width, height, space } = this.props;
		let style = Object.assign({}, { left, top, width, height }, { position: 'absolute', left: left + this.state.position * space });

		return (
			<div>
				<span className="indicator bg-success" style={style} />
			</div>
		);
	}
}

export class Step extends React.Component {
	constructor(...args) {
		super(...args);

		let { alt, note, enabled, positionX, positionY, width, height } = this.props;

		this.color = alt ? StepColorAlt : StepColor;
		this.colorEnabled = alt ? StepEnabledColorAlt : StepEnabledColor;

		this.normalWidth = width;
		this.state = {
			positionX,
			positionY,
			width,
			height,
			enabled,
			note,
			color: enabled ? this.colorEnabled : this.color
		};

		this.mouseClick = this.mouseClick.bind(this);
	}

	mouseClick(lmb, rmb, noteDuration) {
		if (!noteDuration) lmb = window.lmd;
		if (!noteDuration) rmb = window.rmd;

		if (!(lmb ^ rmb) || this.props.sectionEditor.noteDuration == 0) return;

		if (!noteDuration) noteDuration = this.props.sectionEditor.noteDuration;
		let index = this.state.note.bar.notes.indexOf(this.state.note);
		if (this.state.note.bar.notes.length - index < noteDuration) noteDuration = this.state.note.bar.notes.length - index;

		this.setState({
			width: lmb ? (this.normalWidth + this.props.sectionEditor.state.space) * noteDuration - this.props.sectionEditor.state.space : this.normalWidth,
			color: lmb ? this.colorEnabled : this.color
		});

		if (!this.state.note.enabled && lmb) {
			for (let i = index + 1; i < index + noteDuration; i++) {
				this.state.note.bar.notes[i].enabled = false;
				this.state.note.bar.notes[i].step.setState({
					enabled: false,
					width: this.state.note.bar.notes[i].step.normalWidth,
					color: this.state.note.bar.notes[i].step.color
				});
			}
		}

		this.state.note.enabled = lmb;
		Object.assign(this.state.note.properties, { duration: lmb ? noteDuration : 1 });
	}

	render() {
		let { positionX: left, positionY: top, width, height } = this.state;
		let style = Object.assign({}, { left, top, width, height }, { position: 'absolute', backgroundColor: this.state.color, cursor: 'default' });

		return <span className={'btn btn-outline-secondary'} onMouseOver={this.mouseClick} onMouseDown={this.mouseClick} style={style} />;
	}
}

export class PlayerSectionEditor extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = { saveText: 'Save' };

		this.sectionEditor = {
			durationButtons: [],
			noteDuration: 1
		};
	}
	render() {
		let stepHeight = 25;
		return (
			<div hidden={this.props.className == 'hidden'}>
				<div className="container-fluid bg-secondary">
					<div className="row align-items-center">
						<div className="col">
							<PlayerRoundButton
								icon="play_arrow"
								pressedIcon="stop"
								toggle={true}
								on={() => {
									this.sectionEditor.state.section.play();
								}}
								off={() => {
									this.sectionEditor.state.section.stop();
								}}
							/>
						</div>
						<div className="col">
							<PlayerButton
								text={'1'}
								ref={b => (this.sectionEditor.durationButtons = [b])}
								toggle={true}
								pressed={true}
								on={f => {
									for (let b of this.sectionEditor.durationButtons) {
										if (b.state.pressed && !f) b.mouseClick(true);
									}
									if (!f) this.sectionEditor.durationButtons[0].mouseClick(true);
									this.sectionEditor.noteDuration = 1;
								}}
								off={() => {
									this.sectionEditor.noteDuration = 0;
								}}
							/>
							<PlayerButton
								text={'2'}
								ref={b => this.sectionEditor.durationButtons.push(b)}
								toggle={true}
								on={f => {
									for (let b of this.sectionEditor.durationButtons) {
										if (b.state.pressed && !f) b.mouseClick(true);
									}
									if (!f) this.sectionEditor.durationButtons[1].mouseClick(true);
									this.sectionEditor.noteDuration = 2;
								}}
								off={() => {
									this.sectionEditor.noteDuration = 0;
								}}
							/>
							<PlayerButton
								text={'3'}
								ref={b => this.sectionEditor.durationButtons.push(b)}
								toggle={true}
								on={f => {
									for (let b of this.sectionEditor.durationButtons) {
										if (b.state.pressed && !f) b.mouseClick(true);
									}
									if (!f) this.sectionEditor.durationButtons[2].mouseClick(true);
									this.sectionEditor.noteDuration = 3;
								}}
								off={() => {
									this.sectionEditor.noteDuration = 0;
								}}
							/>
							<PlayerButton
								text={'4'}
								ref={b => this.sectionEditor.durationButtons.push(b)}
								toggle={true}
								on={f => {
									for (let b of this.sectionEditor.durationButtons) {
										if (b.state.pressed && !f) b.mouseClick(true);
									}
									if (!f) this.sectionEditor.durationButtons[3].mouseClick(true);
									this.sectionEditor.noteDuration = 4;
								}}
								off={() => {
									this.noteDuration = 0;
								}}
							/>
						</div>
						<div className="col text-right">
							{/*this.state.saveText == 'Save' && (
								<PlayerButton
									text={this.state.saveText}
									pressed={true}
									toggle={false}
									on={() => {
										this.setState({ saveText: 'Saving...' });
										this.props.section.liveTrack.compile(() => {
											if (this.props.onSave) this.props.onSave();
											this.setState({ saveText: 'Save' });
										});
									}}
								/>
							)*/}
							{this.state.saveText != 'Save' && <PlayerButton text={this.state.saveText} pressed={false} toggle={false} on={() => {}} />}
						</div>
					</div>
				</div>

				<div className="container-fluid border border-secondary" style={{ backgroundColor: '#eeeeee', position: 'relative', overflow: 'auto', height: '80vh' }}>
					<SectionEditor
						ref={se => {
							if (!se) return;
							Object.assign(se, this.sectionEditor);
							this.sectionEditor = se;
						}}
						positionX={0}
						positionY={this.props.offset}
						stepWidth={35}
						stepHeight={stepHeight}
						space={1}
						section={this.props.section}
						playerEditor={this}
					/>
				</div>
			</div>
		);
	}
}

export class PlayerEffectsEditor extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = { wavesurfer: null };

		this.fadeInType = this.fadeInType.bind(this);
		this.fadeOutType = this.fadeOutType.bind(this);
		this.fadeOutStart = this.fadeOutStart.bind(this);
	}
	componentDidMount() {
		this.setState({ wavesurfer: WaveSurfer.create({ container: '#waveform' }) }, () => {
			this.state.wavesurfer.load(this.props.url);

			let volumeChange = () => {
				this.props.section.properties.volume = volume.getValue();
			};
			let volume = $('#vol')
				.slider()
				.on('slideStop', volumeChange)
				.data('slider');
			volume.setValue(this.props.section.properties.volume);
			$('#volSlider .slider-selection').css('background', '#BABABA');

			let gainChange = () => {
				this.props.section.properties.gain = gain.getValue();
			};
			let gain = $('#gain')
				.slider()
				.on('slideStop', gainChange)
				.data('slider');
			gain.setValue(this.props.section.properties.gain);
			$('#gainSlider .slider-selection').css('background', '#BABABA');

			let tremoloSpeedChange = () => {
				this.props.section.properties.tremoloSpeed = tremoloSpeed.getValue();
			};
			let tremoloSpeed = $('#trsp')
				.slider()
				.on('slideStop', tremoloSpeedChange)
				.data('slider');
			tremoloSpeed.setValue(this.props.section.properties.tremoloSpeed);
			$('#trspSlider .slider-selection').css('background', '#BABABA');

			let tremoloDepthChange = () => {
				this.props.section.properties.tremoloDepth = tremoloDepth.getValue();
			};
			let tremoloDepth = $('#trde')
				.slider()
				.on('slideStop', tremoloDepthChange)
				.data('slider');
			tremoloDepth.setValue(this.props.section.properties.tremoloDepth);
			$('#trdeSlider .slider-selection').css('background', '#BABABA');

			let trebleChange = () => {
				this.props.section.properties.treble = treble.getValue();
			};
			let treble = $('#trbl')
				.slider()
				.on('slideStop', trebleChange)
				.data('slider');
			treble.setValue(this.props.section.properties.treble);
			$('#trblSlide .slider-selection').css('background', '#BABABA');

			let bassChange = () => {
				this.props.section.properties.bass = bass.getValue();
			};
			let bass = $('#bass')
				.slider()
				.on('slideStop', bassChange)
				.data('slider');
			bass.setValue(this.props.section.properties.bass);
			$('#bassSlide .slider-selection').css('background', '#BABABA');

			let fadeInChange = () => {
				this.props.section.properties.fadeIn = fadeIn.getValue();
			};
			let fadeIn = $('#fin')
				.slider()
				.on('slideStop', fadeInChange)
				.data('slider');
			fadeIn.setValue(this.props.section.properties.fadeIn);
			$('#finSlide .slider-selection').css('background', '#BABABA');

			let fadeOutChange = () => {
				this.props.section.properties.fadeOut = fadeOut.getValue();
			};
			let fadeOut = $('#fou')
				.slider()
				.on('slideStop', fadeOutChange)
				.data('slider');
			fadeOut.setValue(this.props.section.properties.fadeOut);
			$('#fiouSlide .slider-selection').css('background', '#BABABA');
			$('#foustart').val(this.props.section.properties.fadeOutStart || 0);
			$('#fintype').val(this.props.section.properties.fadeInType || 'l');
			$('#foutype').val(this.props.section.properties.fadeOutType || 'l');
		});
	}
	fadeOutStart(e) {
		this.props.section.properties.fadeOutStart = e.target.value;
	}
	fadeInType(e) {
		this.props.section.properties.fadeInType = e.target.value;
	}
	fadeOutType(e) {
		this.props.section.properties.fadeOutType = e.target.value;
	}
	reload() {
		this.state.wavesurfer.load(this.props.url);
	}
	render() {
		return (
			<div>
				<div className="container-fluid bg-secondary">
					<div className="row align-items-center">
						<div className="col">
							<PlayerRoundButton
								icon="play_arrow"
								pressedIcon="stop"
								toggle={true}
								on={() => this.state.wavesurfer.playPause()}
								off={() => this.state.wavesurfer.stop()}
							/>
						</div>
					</div>
				</div>

				<div className="container-fluid border border-secondary" style={{ backgroundColor: '#eeeeee', position: 'relative', overflow: 'auto', height: '80vh' }}>
					<div id="waveform" />
					<br />
					<table className="table table-bordered">
						<tbody>
							<tr>
								<td style={{ width: '300px' }}>
									<label>Volume:</label>{' '}
								</td>
								<td>
									<input id="vol" data-slider-id="volSlider" type="text" data-slider-min="0" data-slider-max="1" data-slider-step="0.01" data-slider-value="1" />
								</td>
							</tr>
							<tr>
								<td style={{ width: '300px' }}>
									<label>Gain:</label>{' '}
								</td>
								<td>
									<input id="gain" data-slider-id="gainSlider" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="0" />
								</td>
							</tr>
							<tr>
								<td style={{ width: '300px' }}>
									<label>Tremolo Frequency:</label>{' '}
								</td>
								<td>
									<input id="trsp" data-slider-id="trspSlider" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="0" />
								</td>
							</tr>
							<tr>
								<td style={{ width: '300px' }}>
									<label>Tremolo Depth:</label>{' '}
								</td>
								<td>
									<input id="trde" data-slider-id="trdeSlider" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="0" />
								</td>
							</tr>
							<tr>
								<td style={{ width: '300px' }}>
									<label>Treble:</label>{' '}
								</td>
								<td>
									<input id="trbl" data-slider-id="trblSlide" type="text" data-slider-min="-50" data-slider-max="50" data-slider-step="1" data-slider-value="0" />
								</td>
							</tr>
							<tr>
								<td style={{ width: '300px' }}>
									<label>Bass:</label>{' '}
								</td>
								<td>
									<input id="bass" data-slider-id="bassSlide" type="text" data-slider-min="-50" data-slider-max="50" data-slider-step="1" data-slider-value="0" />
								</td>
							</tr>
							<tr>
								<td style={{ width: '300px' }}>
									<label>
										Fade in type/duration:{' '}
										<small>
											<i>(in seconds)</i>
										</small>
									</label>{' '}
								</td>
								<td>
									<select id="fintype" className="form-control" onChange={this.fadeInType}>
										<option value="l">Logaritmic</option>
										<option value="t">Linear</option>
										<option value="h">Sine</option>
									</select>
									<br />
									<input id="fin" data-slider-id="finSlide" type="text" data-slider-min="0" data-slider-max="2" data-slider-step="0.01" data-slider-value="0" />
								</td>
							</tr>
							<tr>
								<td style={{ width: '300px' }}>
									<label>
										Fade out type/start/duration:{' '}
										<small>
											<i>(in seconds)</i>
										</small>
									</label>{' '}
								</td>
								<td>
									<select cid="foutype" className="form-control" onChange={this.fadeOutType}>
										<option value="l">Logaritmic</option>
										<option value="t">Linear</option>
										<option value="h">Sine</option>
									</select>
									<br />
									<input id="foustart" className="form-control" type="number" step="0.01" onChange={this.fadeOutStart} />
									<br />
									<input id="fou" data-slider-id="fouSlide" type="text" data-slider-min="0" data-slider-max="2" data-slider-step="0.01" data-slider-value="0" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		);
	}
}

export class PlayerOnly extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = { wavesurfer: null };
	}
	componentDidMount() {
		if (!this.props.id) return;
		this.setState({ wavesurfer: WaveSurfer.create({ container: '#waveform-' + this.props.id }) }, () => {
			this.state.wavesurfer.load(this.props.url);
		});
	}
	reload() {
		this.state.wavesurfer.load(this.props.url);
	}
	render() {
		return (
			<div>
				<div className="container-fluid bg-secondary">
					<div className="row align-items-center">
						<div className="col">
							<PlayerRoundButton
								icon="play_arrow"
								pressedIcon="stop"
								toggle={true}
								on={() => this.state.wavesurfer.playPause()}
								off={() => this.state.wavesurfer.stop()}
							/>
						</div>
					</div>
				</div>

				<div className="container-fluid border border-secondary" style={{ backgroundColor: '#eeeeee', position: 'relative', overflow: 'auto', height: 'auto' }}>
					<div id={'waveform-' + this.props.id} />
				</div>
			</div>
		);
	}
}

export class PlayerTrackEditor extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = { saveText: 'Save' };
	}
	render() {
		let stepHeight = 25;
		return (
			<div hidden={this.props.className == 'hidden'}>
				<div className="container-fluid bg-secondary">
					<div className="row align-items-center">
						<div className="col">
							<PlayerRoundButton
								icon="play_arrow"
								pressedIcon="stop"
								toggle={true}
								on={() => {
									this.sectionEditor.state.section.play();
								}}
								off={() => {
									this.sectionEditor.state.section.stop();
								}}
							/>
						</div>
						<div className="col" />

						<div className="col text-right">
							{/*this.state.saveText == 'Save' && (
								<PlayerButton
									text={this.state.saveText}
									pressed={true}
									toggle={false}
									on={() => {
										this.setState({ saveText: 'Saving...' });
										this.props.onSave();
										this.props.section.liveTrack.compile(() => this.setState({ saveText: 'Save' }));
									}}
								/>
							)*/}
							{this.state.saveText != 'Save' && <PlayerButton text={this.state.saveText} pressed={false} toggle={false} on={() => {}} />}
						</div>
					</div>
				</div>

				<div className="container-fluid border border-secondary" style={{ backgroundColor: '#eeeeee', position: 'relative', overflow: 'auto', height: '80vh' }}>
					<SectionEditor
						ref={se => {
							if (!se) return;
							Object.assign(se, this.sectionEditor);
							this.sectionEditor = se;
						}}
						positionX={0}
						positionY={this.props.offset}
						stepWidth={35}
						stepHeight={stepHeight}
						space={1}
						section={this.props.section}
						add={this.props.add}
						playerEditor={this}
					/>
				</div>
			</div>
		);
	}
}

export class PlayerRoundButton extends Button {
	constructor(...args) {
		super(...args);
	}
	render() {
		let { pressed } = this.state;
		if (this.props.disabled) return <button className="btn btn-success bmd-btn-fab bmd-btn-fab-sm" style={{ margin: '5px', opacity: 0, cursor: 'default' }} />;

		return (
			<button className="btn btn-success bmd-btn-fab bmd-btn-fab-sm" style={{ margin: '5px' }} onMouseDown={this.mouseClick}>
				<i className="material-icons">{pressed ? this.props.pressedIcon : this.props.icon}</i>
			</button>
		);
	}
}

export class PlayerButton extends Button {
	constructor(...args) {
		super(...args);
	}
	render() {
		let { text, pressed } = this.state;
		let style = { margin: '0px', padding: '5px' };

		if (!pressed) Object.assign(style, { backgroundColor: '#eeeeee' });

		return (
			<button className={'btn ' + (pressed ? 'btn-primary btn-raised' : 'btn-outline-primary')} style={style} onMouseDown={this.mouseClick}>
				{text ? text : ''}
			</button>
		);
	}
}
