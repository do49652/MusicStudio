import React from 'react';
import * as jsonpack from 'jsonpack';

import { PlayerOnly } from '../../studio/editor/elements/Player';

const headers = {
	Accept: 'application/json',
	'Content-Type': 'text/plain'
};

export class PostContainer extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			posts: [],
			users: [],
			loading: true
		};

		this.loadMore = this.loadMore.bind(this);
	}
	componentDidMount() {
		this.loadMore();
	}
	loadMore() {
		this.setState({ loading: true });

		let body = jsonpack.pack({
			users: this.props.users,
			skip: this.state.posts.length,
			usersP: this.state.users.map(u => u._id)
		});

		fetch(new Request('/posts-users', { method: 'post', headers, credentials: 'same-origin', body }))
			.then(res => res.text())
			.then(res => this.setState({ posts: this.state.posts.concat(jsonpack.unpack(res).posts), loading: false, users: jsonpack.unpack(res).users }));
	}
	render() {
		return (
			<div>
				{this.state.posts.map(p => <Post key={p._id} post={p} user={this.state.users.filter(u => u._id + '' == p.user + '')[0]} />)}
				<div className="text-center">
					<button onClick={this.loadMore} className={'btn btn-raised btn-primary my-2 my-sm-0' + (this.state.loading ? ' disabled' : '')}>
						{this.state.loading ? 'Loading...' : 'Load more'}
					</button>
				</div>
				<br />
			</div>
		);
	}
}

export class Post extends React.Component {
	constructor(...args) {
		super(...args);
		this.state = { comment: null };
		this.handleChange = this.handleChange.bind(this);
		this.comment = this.comment.bind(this);
	}
	handleChange(e) {
		this.setState({ comment: e.target.value });
	}
	comment() {
		if (!this.state.comment) return;

		let body = jsonpack.pack({
			postId: this.props.post._id,
			comment: this.state.comment
		});

		fetch(new Request('/comment', { method: 'post', headers, credentials: 'same-origin', body }))
			.then(res => res.text())
			.then(res => window.location.reload());
	}
	render() {
		let { user, post } = this.props;

		return (
			<div className="card bg-light border border-primary">
				<div className="card-body" style={{ paddingLeft: '15px', paddingTop: 0, paddingBottom: 0 }}>
					<div className="row">
						{user.me && <div className="w-10 bg-primary" style={{ width: '10px' }} />}

						<div className="col-xs-2" style={{ padding: '20px' }}>
							<img className="img-fluid rounded-circle border border-secondary" src={user.profilePicture || '/img/profile.jpg'} />
						</div>
						<div className="col-xs-9" style={{ padding: '20px' }}>
							<div className="row">
								<div className="col-xs-8">
									<label>
										<a href={'/profile/' + user._id}>{user.username}</a>
									</label>
								</div>
								<div className="col-xs-4 text-right">{post.creationDate.replace('T', ' ').split('.')[0]}</div>
							</div>
							<div>{post.text}</div>
							<br />
							{post.song && <PlayerOnly id={post._id} url={post.song} />}
						</div>
						<button className="btn btn-outline-primary form-control" data-toggle="collapse" data-target={'#comments-' + post._id} style={{ marginLeft: '15px' }}>
							Comments ({post.comments ? post.comments.length : 0})
						</button>
					</div>
				</div>
				<div className="card-footer bg-light" style={{ padding: 0, margin: 0 }}>
					<div id={'comments-' + post._id} className="collapse" style={{ margin: '10px', marginLeft: '20px' }}>
						{post.comments &&
							post.comments.map(c => (
								<div key={c.time} className={'alert alert-primary border border-primary'}>
									<div className="row">
										<div className="col">
											<h5 className="alert-heading">{c.username}</h5>
										</div>
										<div className="col text-right">
											{c.time
												.toString()
												.replace('T', ' ')
												.replace(/\..*$/, '')}
										</div>
									</div>
									<div className="row" style={{ marginLeft: '25px' }}>
										<p>{c.comment}</p>
									</div>
								</div>
							))}
						<div className="alert border border-primary">
							<div className="input-group">
								<input className="form-control" placeholder="Comment..." onChange={this.handleChange} />
								<div className="input-group-append">
									<button className="btn btn-primary btn-raised" onClick={this.comment}>
										Comment
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export class NewPost extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			posting: false,
			song: null
		};

		this.text = null;
		this.post = this.post.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}
	post() {
		this.setState({ posting: true });

		let body = { text: this.text.value };
		let { song } = this.state;
		if (song && song != '0') Object.assign(body, { song });

		fetch(new Request('/post', { method: 'post', headers, credentials: 'same-origin', body: jsonpack.pack(body) }))
			.then(res => res.text())
			.then(res => window.location.reload());
	}
	handleChange(event) {
		let { options } = event.target;
		if (!options) return;

		options = [...Array(options.length).keys()].map(i => options[i]).filter(o => o.selected);
		if (options.length == 0) return;

		let song = options[0].value;
		this.setState({ song });
	}
	render() {
		return (
			<div className={this.props.className}>
				<div className="card-body">
					<div className="row">
						<div className="col-xs-3">
							<img className="img-fluid" src={this.props.user.profilePicture || '/img/profile.jpg'} />
						</div>
						<div className="col-xs-9">
							<textarea ref={t => (this.text = t)} className={'form-control' + (this.state.posting ? ' disabled' : '')} rows="5" placeholder="New post..." />
							<div className="text-right">
								<a className="btn btn-primary" data-toggle="collapse" href="#attach-song" role="button">
									Select song
								</a>
							</div>
							<div className="collapse" id="attach-song">
								<select className="form-control" onChange={this.handleChange}>
									<option value="0">None</option>
									{this.props.user.songs.map(s => (
										<option key={s.id} value={s.id}>
											{s.name}
										</option>
									))}
								</select>
							</div>
							<br />
							<div className="text-right">
								<button onClick={this.post} className={'btn btn-raised btn-primary my-2 my-sm-0' + (this.state.posting ? ' disabled' : '')}>
									Post
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
