import React from 'react';

export default class Register extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			username: {
				hasError: false
			},
			email: {
				hasError: false
			},
			password: {
				value: '',
				hasError: false
			},
			password2: {
				hasError: false
			}
		};

		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(event) {
		let { name, value } = event.target;

		if (name == 'username') {
			this.setState({
				username: {
					hasError: !/^[A-Za-z0-9]*$/.test(value)
				}
			});
		}

		if (name == 'email') {
			this.setState({
				email: {
					hasError: !/(.+)@(.+){2,}\.(.+){2,}/.test(value)
				}
			});
		}

		if (name == 'password') {
			this.setState({
				password: {
					value,
					hasError: !/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/.test(value)
				}
			});
		}

		if (name == 'password2') {
			this.setState({
				password2: {
					hasError: this.state.password.value != value
				}
			});
		}
	}
	render() {
		let { username, email, password, password2 } = this.state;
		return (
			<div className="card">
				<div className="card-block">
					<h4 className="card-title">Register</h4>
					<form action="/register" method="post">
						<fieldset className={'form-group' + (username.hasError ? ' has-danger' : '')}>
							<label htmlFor="usernameRegInput" className="bmd-label-floating">
								Username
							</label>

							<input
								type="text"
								name="username"
								className={'form-control' + (username.hasError ? ' form-control-danger' : '')}
								id="usernameRegInput"
								onChange={this.handleChange}
							/>

							{username.hasError && <div className="form-control-feedback">Use only alphanumeric characters.</div>}
							<small className="bmd-help">Use only alphanumeric characters.</small>
						</fieldset>

						<fieldset className={'form-group' + (email.hasError ? ' has-danger' : '')}>
							<label htmlFor="emailRegInput" className="bmd-label-floating">
								E-mail
							</label>

							<input
								type="text"
								name="email"
								className={'form-control' + (email.hasError ? ' form-control-danger' : '')}
								id="emailRegInput"
								onChange={this.handleChange}
							/>

							{email.hasError && <div className="form-control-feedback">Enter valid e-mail.</div>}
							<small className="bmd-help">Enter valid e-mail.</small>
						</fieldset>

						<fieldset className={'form-group' + (password.hasError ? ' has-danger' : '')}>
							<label htmlFor="passRegInput" className="bmd-label-floating">
								Password
							</label>

							<input
								type="password"
								name="password"
								className={'form-control' + (password.hasError ? ' form-control-danger' : '')}
								id="passRegInput"
								onChange={this.handleChange}
							/>

							{password.hasError && (
								<div className="form-control-feedback">Use only alphanumeric characters. Minimum of 8 characters, one letter and one number.</div>
							)}
							<small className="bmd-help">Use only alphanumeric characters. Minimum of 8 characters, one letter and one number.</small>
						</fieldset>

						<fieldset className={'form-group' + (password2.hasError ? ' has-danger' : '')}>
							<label htmlFor="passRegInput2" className="bmd-label-floating">
								Confirm password
							</label>

							<input
								type="password"
								name="password2"
								className={'form-control' + (password2.hasError ? ' form-control-danger' : '')}
								id="passRegInput2"
								onChange={this.handleChange}
							/>

							{password2.hasError && (
								<div className="form-control-feedback">Use only alphanumeric characters. Minimum of 8 characters, one letter and one number.</div>
							)}
							<small className="bmd-help">Use only alphanumeric characters. Minimum of 8 characters, one letter and one number.</small>
						</fieldset>

						<button type="submit" className="btn btn-raised btn-primary">
							Register
						</button>
					</form>
				</div>
			</div>
		);
	}
}
