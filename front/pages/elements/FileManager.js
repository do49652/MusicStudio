import React from 'react';
import * as jsonpack from 'jsonpack';
import { PlayerOnly } from '../../studio/editor/elements/Player';

const headers = {
	Accept: 'application/json',
	'Content-Type': 'text/plain'
};

export default class FileManager extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			songs: null,
			songName: { hasError: false, value: 'Song' },
			bpm: { hasError: false, value: 120 },
			bars: { hasError: false, value: 4 },
			beats: { hasError: false, value: 4 },
			creating: false,
			deleting: null
		};

		this.handleChange = this.handleChange.bind(this);
		this.newSong = this.newSong.bind(this);
		this.deleteSong = this.deleteSong.bind(this);
	}
	componentDidMount() {
		fetch(new Request('/songs', { method: 'post', credentials: 'same-origin' }))
			.then(res => res.text())
			.then(res => this.setState({ songs: jsonpack.unpack(res) }));
	}
	handleChange(event) {
		let { name, value } = event.target;
		if (name == 'songName') this.setState({ songName: { hasError: !/^[A-Za-z0-9 -_]*$/.test(value), value } });
		if (name == 'bpm') this.setState({ bpm: { hasError: !(value > 0 && value < 261), value } });
		if (name == 'bars') this.setState({ bars: { hasError: !(value > 0 && value < 17), value } });
		if (name == 'beats') this.setState({ beats: { hasError: !(value > 0 && value < 17), value } });
	}
	deleteSong(id) {
		let body = jsonpack.pack({ id });
		fetch(new Request('/delete-song', { method: 'post', headers, credentials: 'same-origin', body })).then(res => window.location.reload());
	}
	newSong() {
		if (this.state.songName.hasError || this.state.bars.hasError || this.state.beats.hasError) return;
		if (!this.state.songName.value || !this.state.bars.value || !this.state.beats.value) return;

		this.setState({ creating: true });

		let columns = this.state.bars.value * this.state.beats.value;

		let body = jsonpack.pack({
			name: this.state.songName.value,
			song: {
				bpm: this.state.bpm.value,
				bars: this.state.bars.value,
				beats: this.state.beats.value,
				sections: [],
				track: {
					name: 'Track',
					instrument: null,
					properties: { volume: 1.0, pitch: 0.0, bpm: this.state.bpm.value / columns, duration: 1 },
					columns,
					id: new Buffer(this.state.songName.value + '_' + new Date().getTime() + '_Track').toString()
				}
			}
		});

		fetch(new Request('/new-song', { method: 'post', headers, credentials: 'same-origin', body }))
			.then(res => res.text())
			.then(res => (window.location = '/editor/' + res.replace(/"/g, '')));
	}
	render() {
		let { songs, songName, bpm, bars, beats, deleting } = this.state;
		if (deleting) {
			return <div className="container">Deleting {songs.filter(s => s._id == deleting)[0].name}</div>;
		}
		return (
			<div className="container-fluid">
				<button data-toggle="modal" data-target="#new-song" data-backdrop="false" className="btn btn-raised btn-primary my-2 my-sm-0">
					New song
				</button>
				<br />
				<br />

				{!songs && <h4>Loading...</h4>}

				{songs && songs.length == 0 && <h4>You haven't made a song yet</h4>}

				{songs &&
					songs.length > 0 && (
						<table className="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>Song name</th>
									<th>Creation Date</th>
									<th>Properties</th>
									<th>Audio</th>
									<th />
								</tr>
							</thead>
							<tbody>
								{songs.map(s => (
									<tr key={s._id}>
										<td>
											<a href={'/editor/' + s._id}>{s.name}</a>
										</td>
										<td>{s.creationDate.replace('T', ' ').replace(/\..*$/, '')}</td>
										<td>
											<ul>
												<li>BPM: {s.song.bpm}</li>
												<li>Bars per section: {s.song.bars}</li>
												<li>Notes per section: {s.song.beats}</li>
											</ul>
										</td>
										<td style={{ width: '40vh' }}>
											<PlayerOnly id={s._id} url={'/instruments/custom/' + s.user + '/' + s.song.track.id + '.wav'} />
										</td>
										<td>
											<button
												className="btn btn-danger btn-raised"
												onClick={() => confirm('Are you sure?') && this.setState({ deleting: s._id }, () => this.deleteSong(s._id))}
											>
												Delete
											</button>
										</td>
									</tr>
								))}
							</tbody>
						</table>
					)}

				<div id="new-song" className="modal fade" tabIndex="-1" role="dialog">
					<div className="modal-dialog modal-lg modal-dialog-centered" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title" id="new-song">
									Create a new song
								</h5>
								<button type="button" className="close" data-dismiss="modal">
									<span>&times;</span>
								</button>
							</div>

							<div className="modal-body">
								<div className="">
									<fieldset className={'form-group' + (songName.hasError ? ' has-danger' : '')}>
										<label htmlFor="songNameInput" className="bmd-label-floating">
											Song name
										</label>

										<input
											type="text"
											name="songName"
											className={'form-control' + (songName.hasError ? ' form-control-danger' : '')}
											id="songNameInput"
											onChange={this.handleChange}
											defaultValue={this.state.songName.value}
											disabled={this.state.creating}
										/>

										{songName.hasError && <div className="form-control-feedback">Use only alphanumeric characters.</div>}
										<small className="bmd-help">Use only alphanumeric characters.</small>
									</fieldset>
								</div>
								<div className="form-row col align-self-center">
									<fieldset className={'form-group' + (bpm.hasError ? ' has-danger' : '')}>
										<label htmlFor="bpmInput" className="bmd-label-floating">
											BPM
										</label>

										<input
											type="number"
											name="bpm"
											className={'form-control' + (bpm.hasError ? ' form-control-danger' : '')}
											id="bpmInput"
											onChange={this.handleChange}
											defaultValue={this.state.bpm.value}
											disabled={this.state.creating}
										/>

										{bpm.hasError && <div className="form-control-feedback">BPM must be between 1 and 260.</div>}
										<small className="bmd-help">BPM must be between 1 and 260.</small>
									</fieldset>
									<br />
									<fieldset className={'form-group' + (bars.hasError ? ' has-danger' : '')}>
										<label htmlFor="barsInput" className="bmd-label-floating">
											Number of bars per section
										</label>

										<input
											type="number"
											name="bars"
											className={'form-control' + (bars.hasError ? ' form-control-danger' : '')}
											id="barsInput"
											onChange={this.handleChange}
											defaultValue={this.state.bars.value}
											disabled={this.state.creating}
										/>

										{bars.hasError && <div className="form-control-feedback">Number of bars must be between 1 and 16.</div>}
										<small className="bmd-help">Number of bars must be between 1 and 16.</small>
									</fieldset>
									<br />
									<fieldset className={'form-group' + (beats.hasError ? ' has-danger' : '')}>
										<label htmlFor="beatsInput" className="bmd-label-floating">
											Number of notes per bar
										</label>

										<input
											type="number"
											name="beats"
											className={'form-control' + (beats.hasError ? ' form-control-danger' : '')}
											id="beatsInput"
											onChange={this.handleChange}
											defaultValue={this.state.beats.value}
											disabled={this.state.creating}
										/>

										{beats.hasError && <div className="form-control-feedback">Number of notes must be between 1 and 16.</div>}
										<small className="bmd-help">Number of notes must be between 1 and 16.</small>
									</fieldset>
								</div>
							</div>

							<div className="modal-footer">
								<button type="button" className="btn btn-secondary" data-dismiss="modal" disabled={this.state.creating}>
									Close
								</button>
								<button type="button" className="btn btn-primary" onClick={this.newSong} disabled={this.state.creating}>
									{this.state.creating ? 'Creating...' : 'Create'}
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
