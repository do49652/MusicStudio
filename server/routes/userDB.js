import mongoose, { mongo } from 'mongoose';
import { User } from './models';
import { mongodb } from '../properties';

export const getUser = (username, password, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ username, password }, (err, users) => callback(users.length == 0 ? null : users[0]));
	});
};

export const getUserById = (id, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ _id: id }, (err, users) => callback(users.length == 0 ? null : users[0]));
	});
};

export const getUsersById = (ids, callback) => {
	mongoose.connect(mongodb, () => {
		User.find(ids ? { _id: { $in: ids } } : {}, (err, users) => callback(users));
	});
};

export const getUsersByQuery = (query, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ username: { $regex: query, $options: 'i' } }, (err, users) => callback(users));
	});
};

export const followUser = (idA, idB, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ _id: idA }, (err, usersA) => {
			if (usersA.length == 0) return callback('lenA0');
			let userA = usersA[0];

			User.find({ _id: idB }, (err, usersB) => {
				if (usersB.length == 0) return callback('lenB0');
				let userB = usersB[0];

				userA.following.push(userB._id);
				userB.followers.push(userA._id);

				userA.save((err, product) => {
					if (err) {
						callback(err);
						return console.log(err);
					}
					userB.save((err, product) => {
						if (err) {
							callback(err);
							return console.log(err);
						}
						callback(null);
					});
				});
			});
		});
	});
};

export const unfollowUser = (idA, idB, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ _id: idA }, (err, usersA) => {
			if (usersA.length == 0) return callback('lenA0');
			let userA = usersA[0];

			User.find({ _id: idB }, (err, usersB) => {
				if (usersB.length == 0) return callback('lenB0');
				let userB = usersB[0];

				let indexA = userA.following.indexOf(userB._id);
				let indexB = userB.followers.indexOf(userA._id);

				userA.following.splice(indexA, 1);
				userB.followers.splice(indexB, 1);

				userA.save((err, product) => {
					if (err) {
						callback(err);
						return console.log(err);
					}
					userB.save((err, product) => {
						if (err) {
							callback(err);
							return console.log(err);
						}
						callback(null);
					});
				});
			});
		});
	});
};

export const addUser = (email, username, password, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ username }, (err, users) => {
			if (users.length != 0) return callback();
			let user = new User({ username, email, password });
			user.save((err, product) => {
				if (err) return console.log(err);
				callback();
			});
		});
	});
};

export const editUser = (id, changes, callback) => {
	mongoose.connect(mongodb, () => {
		User.find({ _id: id }, (err, users) => {
			if (users.length == 0) return callback(false);

			let user = users[0];
			Object.assign(user, changes);

			user.save((err, product) => {
				if (err) return console.log(err);
				callback(true);
			});
		});
	});
};
