//var webpack = require('webpack');
//var CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
	entry: ['./app.js'],
	output: {
		path: __dirname + '/../server/public',
		filename: 'bundle.js'
	},
	module: {
		rules: [
			{
				test: /\.js?$/,
				loader: 'babel-loader',
				exclude: /(node_modules)/,
				options: {
					presets: ['react', 'env', 'stage-0']
				}
			}
		]
	},
	watch: true,
	plugins: [
		/*
		new CompressionPlugin({
			asset: '[path].gz[query]',
			algorithm: 'gzip',
			test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
			threshold: 10240,
			minRatio: 0.8
		})*/
	],
	performance: { hints: false },
	mode: 'development'
};
