import React from 'react';
import ReactDOM from 'react-dom';
import { TopBar, Button, Indicator, Background, BarProperties, Step, PlayerTrackEditor } from './elements/Player';
import Section from '../audio/Section';

export default class SectionEditor extends React.Component {
	constructor(...args) {
		super(...args);

		let { positionX, positionY, stepWidth, stepHeight, space, section } = this.props;
		this.state = {
			grid: [],
			barProperties: [],
			barPropertiesButtons: [],
			positionX,
			positionY,
			stepWidth,
			stepHeight,
			space,
			section,
			editor: null
		};

		this.barPropertiesRendered = [];
		this.barPropertiesRenderedButtons = [];
		this.durationButtons = [];
		this.noteDuration = 1;
		this.grid = [];

		this.set = this.set.bind(this);
		this.state = Object.assign(this.state, this.set(this.props.section));
	}
	resize() {
		if (this.state.section.section) this.state.section.load();
	}
	set(section) {
		if (!section) return;

		let grid = [];
		let barProperties = [];
		let barPropertiesButtons = [];

		let { positionX, positionY, stepWidth, stepHeight, space } = this.state;

		this.grid = [];
		for (let y = 0; y < section.bars.length; y++) {
			let row = [];
			barProperties.push(
				<BarProperties
					key={section.id + '_bar_properties' + y + '_' + section.bars[y].name}
					ref={bpb => (this.barPropertiesRendered[y] = bpb)}
					positionX={positionX + space * 2 + stepWidth * 4 + space}
					positionY={positionY + stepHeight / 4 + 2 * space + (stepHeight + space) * y + space / 2}
					width={400}
					height={200}
					properties={section.bars[y].properties}
				>
					<div className="card">
						<div className="card-block">
							<div className="card-title">
								<div className="row">
									<div className="col">
										<h4>Properties</h4>
									</div>
									<div className="col text-right">
										{this.props.playerEditor instanceof PlayerTrackEditor && (
											<button
												className="btn btn-primary btn-raised"
												onClick={() => {
													if (this.state.editor) this.state.editor.setState({ shownSection: y });
													if (this.barPropertiesRendered[y]) this.barPropertiesRendered[y].close();
													if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
												}}
											>
												Show
											</button>
										)}
									</div>
								</div>
							</div>
							<div className="card-body">
								<div className="btn-group" role="group" style={{ margin: 0 }}>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												section.bars[y].notes[x].step.mouseClick(false, true, 1);
												section.bars[y].notes[x].properties = section.bars[y].properties;
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										Clear
									</button>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												section.bars[y].notes[x].step.mouseClick(true, false, 1);
												section.bars[y].notes[x].properties = section.bars[y].properties;
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										Fill all
									</button>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											let clipboard = [];
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												clipboard.push({
													enabled: section.bars[y].notes[x].enabled,
													properties: section.bars[y].notes[x].properties
												});
											}
											clipboard = clipboard.reverse();
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												section.bars[y].notes[x].step.mouseClick(false, true, 1);
												section.bars[y].notes[x].properties = section.bars[y].properties;
												if (clipboard[x].enabled) {
													section.bars[y].notes[x].step.mouseClick(true, false, clipboard[x].properties.duration);
													section.bars[y].notes[x].properties = clipboard[x].properties;
												}
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										Reverse
									</button>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												section.bars[y].notes[x].step.mouseClick(!section.bars[y].notes[x].enabled, section.bars[y].notes[x].enabled, 1);
												section.bars[y].notes[x].properties = section.bars[y].properties;
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										Invert
									</button>
								</div>
								<br />
								<div className="btn-group" role="group" style={{ margin: 0 }}>
									<a className="btn btn-sm btn-link" style={{ cursor: 'default' }}>
										Shift:
									</a>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											let clipboard = [];
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												clipboard.push({
													enabled: section.bars[y].notes[x].enabled,
													properties: section.bars[y].notes[x].properties
												});
											}
											for (let x = section.bars[y].notes.length - 2; x >= 0; x--) {
												section.bars[y].notes[x].step.mouseClick(clipboard[x + 1].enabled, !clipboard[x + 1].enabled, clipboard[x + 1].properties.duration);
												section.bars[y].notes[x].properties = clipboard[x + 1].properties;
											}
											section.bars[y].notes[section.bars[y].notes.length - 1].step.mouseClick(false, true, 1);
											section.bars[y].notes[section.bars[y].notes.length - 1].properties = section.bars[y].properties;
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										Left
									</button>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											let clipboard = [];
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												clipboard.push({
													enabled: section.bars[y].notes[x].enabled,
													properties: section.bars[y].notes[x].properties
												});
											}
											for (let x = 1; x < section.bars[y].notes.length; x++) {
												section.bars[y].notes[x].step.mouseClick(clipboard[x - 1].enabled, !clipboard[x - 1].enabled, clipboard[x - 1].properties.duration);
												section.bars[y].notes[x].properties = clipboard[x - 1].properties;
											}
											section.bars[y].notes[0].step.mouseClick(false, true, 1);
											section.bars[y].notes[0].properties = section.bars[y].properties;
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										Right
									</button>
								</div>
								<br />
								<div className="btn-group" role="group" style={{ margin: 0 }}>
									<a className="btn btn-sm btn-link" style={{ cursor: 'default' }}>
										Fill each:
									</a>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											for (let x = 0; x < section.bars[y].notes.length; x += 2) {
												section.bars[y].notes[x].step.mouseClick(true, false, 1);
												section.bars[y].notes[x].properties = section.bars[y].properties;
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										2 steps
									</button>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											for (let x = 0; x < section.bars[y].notes.length; x += 4) {
												section.bars[y].notes[x].step.mouseClick(true, false, 1);
												section.bars[y].notes[x].properties = section.bars[y].properties;
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										4 steps
									</button>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											for (let x = 0; x < section.bars[y].notes.length; x += 8) {
												section.bars[y].notes[x].step.mouseClick(true, false, 1);
												section.bars[y].notes[x].properties = section.bars[y].properties;
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										8 steps
									</button>
								</div>
								<br />
								<div className="btn-group" role="group" style={{ margin: 0 }}>
									<a className="btn btn-sm btn-link" style={{ cursor: 'default' }}>
										Copy:
									</a>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											this.props.playerEditor.props.editor.clipboard = [];
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												this.props.playerEditor.props.editor.clipboard.push({
													enabled: section.bars[y].notes[x].enabled,
													duration: section.bars[y].notes[x].properties.duration
												});
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										Row
									</button>
									{this.props.playerEditor instanceof PlayerTrackEditor && (
										<button
											className="btn btn-sm btn-primary"
											onClick={() => {
												this.props.playerEditor.props.editor.sectionClipboard = [];
												for (let yy = 0; yy < this.props.playerEditor.props.editor.sectionEditors[y].props.section.bars.length; yy++) {
													this.props.playerEditor.props.editor.sectionClipboard.push([]);
													for (let x = 0; x < this.props.playerEditor.props.editor.sectionEditors[y].props.section.bars[yy].notes.length; x++) {
														this.props.playerEditor.props.editor.sectionClipboard[yy].push({
															enabled: this.props.playerEditor.props.editor.sectionEditors[y].props.section.bars[yy].notes[x].enabled,
															duration: this.props.playerEditor.props.editor.sectionEditors[y].props.section.bars[yy].notes[x].properties.duration
														});
													}
												}
												if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
											}}
										>
											Entire section
										</button>
									)}
								</div>
								<br />
								<div className="btn-group" role="group" style={{ margin: 0 }}>
									<a className="btn btn-sm btn-link" style={{ cursor: 'default' }}>
										Paste:
									</a>
									<button
										className="btn btn-sm btn-primary"
										onClick={() => {
											if (!this.props.playerEditor.props.editor.clipboard) return;
											for (let x = 0; x < section.bars[y].notes.length; x++) {
												section.bars[y].notes[x].step.mouseClick(false, true, 1);
												if (this.props.playerEditor.props.editor.clipboard[x].enabled)
													section.bars[y].notes[x].step.mouseClick(true, false, this.props.playerEditor.props.editor.clipboard[x].duration);
											}
											if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
										}}
									>
										Row
									</button>
									{this.props.playerEditor instanceof PlayerTrackEditor && (
										<button
											className="btn btn-sm btn-primary"
											onClick={() => {
												if (!this.props.playerEditor.props.editor.sectionClipboard) return;
												for (let yy = 0; yy < this.props.playerEditor.props.editor.sectionEditors[y].props.section.bars.length; yy++) {
													for (let x = 0; x < this.props.playerEditor.props.editor.sectionEditors[y].props.section.bars[yy].notes.length; x++) {
														this.props.playerEditor.props.editor.sectionEditors[y].props.section.bars[yy].notes[x].step.mouseClick(false, true, 1);
														if (this.props.playerEditor.props.editor.sectionClipboard[yy][x].enabled) {
															this.props.playerEditor.props.editor.sectionEditors[y].props.section.bars[yy].notes[x].step.mouseClick(
																true,
																false,
																this.props.playerEditor.props.editor.sectionClipboard[yy][x].duration
															);
														}
													}
												}
												if (this.barPropertiesRenderedButtons[y]) this.barPropertiesRenderedButtons[y].mouseClick(false);
												this.props.playerEditor.props.editor.setState({ shownSection: y });
											}}
										>
											Entire section
										</button>
									)}
								</div>
							</div>
						</div>
					</div>
				</BarProperties>
			);
			barPropertiesButtons.push(
				<Button
					key={section.id + '_bar_properties_buttons' + y + '_' + section.bars[y].name}
					ref={b => (this.barPropertiesRenderedButtons[y] = b)}
					text={section.bars[y].name}
					positionX={positionX + space * 2}
					positionY={positionY + stepHeight / 4 + 2 * space + (stepHeight + space) * y + space / 2}
					width={stepWidth * 4}
					height={stepHeight - space}
					toggle={true}
					on={() => {
						if (!this.barPropertiesRendered[y]) return;
						this.barPropertiesRendered[y].open();
					}}
					off={() => {
						if (!this.barPropertiesRendered[y]) return;
						this.barPropertiesRendered[y].close();
					}}
				/>
			);
			for (let x = section.columns - 1; x >= 0; x--) {
				let note = section.getNote(y, x);
				row.push(
					<Step
						key={section.id + '_step_row' + y + '_column' + x + '_' + section.bars[y].name}
						ref={s => {
							this.grid.push(s);
							note.step = s;
						}}
						positionX={positionX + space * 2 + stepWidth * 4 + space + (stepWidth + space) * x}
						positionY={positionY + stepHeight / 4 + 2 * space + (stepHeight + space) * y}
						width={stepWidth}
						height={stepHeight}
						enabled={note.enabled}
						note={note}
						sectionEditor={this}
						alt={parseInt(x / 4) % 2 == 1}
					/>
				);
			}
			grid.push(row);
		}

		if (section.section) section.load();
		return { grid, barProperties, barPropertiesButtons, section };
	}
	render() {
		let { positionX, positionY, stepWidth, stepHeight, space, section } = this.state;

		if (!section) return <div />;
		return (
			<div>
				<Indicator
					ref={i => (this.state.section.indicator = i)}
					positionX={positionX + space * 2 + stepWidth * 4 + space}
					positionY={positionY + space}
					width={stepWidth}
					height={stepHeight / 4}
					position={0}
					maxPosition={section.columns}
					space={stepWidth + space}
				/>
				{this.state.grid.length != 0 && this.state.grid.reduce((r, c) => r.concat(c))}
				{this.state.barProperties}
				{this.state.barPropertiesButtons}
				{this.props.add && (
					<Button
						text={'Add'}
						positionX={positionX + space * 2}
						positionY={positionY + stepHeight / 4 + 2 * space + (stepHeight + space) * this.state.barPropertiesButtons.length + space / 2}
						width={stepWidth * 4}
						height={stepHeight - space}
						toggle={false}
						on={this.props.add}
					/>
				)}
			</div>
		);
	}
}

document.addEventListener('contextmenu', event => event.preventDefault());
document.addEventListener('dragenter', event => event.preventDefault(), false);

window.rmd = false;
window.lmd = false;
document.body.onmousedown = event => {
	window.rmd = event.button == 2;
	window.lmd = event.button == 0;
};
document.body.onmouseup = event => {
	window.rmd = window.rmd && window.rmd && event.button == 2 ? false : window.rmd;
	window.lmd = window.lmd && event.button == 0 ? false : window.lmd;
};
