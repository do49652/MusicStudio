import Buffer from './utils/Buffer';
import Crunker from './utils/crunker';
import Sound from './utils/Sound';
import * as jsonpack from 'jsonpack';

export class LiveTrack {
	constructor(section, callback) {
		this.section = section;

		this.ready = false;
		this.buffer = null;
		this.compiled = null;
		this.onCompiling = null;
		this.currentPosition = 0;

		if (section.bars.length != 0) {
			this.buffer = new Buffer(window.audioContext, section.bars.map(b => b.sound));
			this.buffer.loadSound(0, () => this.load(0, 0, callback));
		} else {
			callback();
		}
	}
	playPosition(p) {
		this.currentPosition = p;
		if (!this.ready) return;
		for (let i = 0; i < this.section.bars.length; i++) {
			if (!this.section.bars[i].notes[p].enabled) continue;
			this.section.bars[i].notes[p].sound.play();
		}
	}
	stop() {
		for (let i = 0; i < this.section.bars.length; i++) {
			if (!this.section.bars[i].notes[this.currentPosition].enabled) continue;
			this.section.bars[i].notes[this.currentPosition].sound.stop();
		}
		this.currentPosition = 0;
	}
	load(i, j, callback, only) {
		if (this.buffer == null) return;

		this.section.bars[i].notes[j].sound = new Sound(this.buffer.context, this.buffer.getSound(i), this.section.bars[i].notes[j].properties);
		let allBars = i + 1 == this.section.bars.length;
		let allNotes = j + 1 == this.section.bars[i].notes.length;

		if (only || (allBars && allNotes)) {
			this.ready = true;
			callback && callback();
		} else if (allNotes) this.load(i + 1, 0, callback);
		else this.load(i, j + 1, callback);
	}
	compile(callback) {
		if (this.buffer == null || !this.ready) return;

		let sectionToSend = {
			name: this.section.name,
			id: this.section.id,
			urls: this.buffer.urls,
			properties: this.section.properties,
			bars: this.section.bars.map(b => ({
				name: b.name,
				notes: b.notes.map(n => ({
					properties: { duration: n.properties.duration, pitch: n.properties.pitch },
					enabled: n.enabled
				}))
			}))
		};

		let sectionData = jsonpack.pack({
			wav: sectionToSend,
			song: this.section.save(),
			sections: this.section.tracks.map(t => Object.assign({}, t.save(), { sounds: null }, { sounds: [] }))
		});

		const headers = {
			Accept: 'application/json',
			'Content-Type': 'text/plain'
		};

		fetch(new Request('/save-song', { method: 'post', headers, credentials: 'same-origin', body: sectionData })).then(res => {
			if (this.onCompiling) this.onCompiling(callback);
			else if (callback) callback();
		});
	}
}
