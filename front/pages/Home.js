import React from 'react';
import Header from './elements/Header';
import * as jsonpack from 'jsonpack';
import { NewPost, Post, PostContainer } from './elements/Post';

const headers = {
	Accept: 'application/json',
	'Content-Type': 'text/plain'
};

export default class Home extends React.Component {
	constructor(...args) {
		super(...args);

		this.state = {
			user: null
		};
	}
	componentDidMount() {
		fetch(new Request('/me', { method: 'post', headers, credentials: 'same-origin' }))
			.then(res => res.text())
			.then(res => this.setState({ user: jsonpack.unpack(res) }));
	}
	render() {
		return (
			<div>
				<Header title={'Music Studio'} currentPage={'Home'} />
				<div className="container">
					{this.state.user && (
						<div className={'bg-light border border-primary text-center'}>
							<a className="btn btn-primary" data-toggle="collapse" href="#postNewPost">
								<h5 style={{ margin: '5px' }}>
									<b>POST</b>
								</h5>
							</a>
							<div className="collapse" id="postNewPost">
								<NewPost user={this.state.user} />
							</div>
						</div>
					)}
					<br />
					<div className="container">{this.state.user && <PostContainer users={this.state.user.following.concat([this.state.user._id])} />}</div>
				</div>
			</div>
		);
	}
}
