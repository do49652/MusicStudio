export default class Sound {
	constructor(context, buffer, properties) {
		this.context = context;
		this.buffer = buffer;
		this.properties = properties;
	}
	play(f) {
		this.source = this.context.createBufferSource();
		this.source.playbackRate.setValueAtTime(Math.pow(2, this.properties.pitch / 12), this.context.currentTime);
		this.source.buffer = this.buffer;

		this.gainNode = this.context.createGain();
		this.gainNode.gain.setValueAtTime(this.properties.volume, this.context.currentTime);
		this.gainNode.connect(this.context.destination);
		this.source.connect(this.gainNode);

		if (!f) {
			this.source.start(this.context.currentTime);
			this.source.stop(this.context.currentTime + this.properties.duration * 60 / 4 / this.properties.bpm);
		}
	}
	stop() {
		this.gainNode.gain.setValueAtTime(0, this.context.currentTime);
	}
}
