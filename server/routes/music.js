import express from 'express';
import fs, { stat } from 'fs';
import * as jsonpack from 'jsonpack';

import * as MusicDB from './musicDB';
import * as MusicProcessor from '../musicProcessor';

const router = express.Router();
export default router;

let createFolder = (req, res, next) => {
	if (!req.user) return null;
	let dir = __dirname + '/../../public/instruments/custom/' + req.user._id + '/';
	if (!fs.existsSync(dir)) fs.mkdirSync(dir);
	return next();
};

router.post('/new-song', (req, res) => MusicDB.addSong(req.user._id, jsonpack.unpack(req.body).name, jsonpack.unpack(req.body).song, id => res.json(id)));
router.post('/songs', (req, res) => MusicDB.getSongs(jsonpack.unpack(req.body).users || [req.user._id], songs => res.json(jsonpack.pack(songs))));
router.post('/get-song', (req, res) => MusicDB.getSong(jsonpack.unpack(req.body).id, song => res.json(jsonpack.pack(song))));
router.post('/save-song', createFolder, (req, res) =>
	MusicDB.saveSong(req.user._id, { song: jsonpack.unpack(req.body).song, sections: jsonpack.unpack(req.body).sections }, status => {
		MusicProcessor.compile(req.user._id, jsonpack.unpack(req.body).wav, () => res.json(status));
	})
);
router.post('/delete-song', (req, res) => MusicDB.deleteSong(jsonpack.unpack(req.body).id, status => res.json(status)));
