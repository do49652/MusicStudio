import mongoose, { Schema } from 'mongoose';

const arrayOfOids = { type: [Schema.Types.ObjectId], default: [] };

const userSchema = new Schema({
	username: String,
	email: String,
	password: String,
	profilePicture: { type: String, default: null },
	creationDate: { type: Date, default: Date.now },
	songs: arrayOfOids,
	posts: arrayOfOids,
	likedPosts: arrayOfOids,
	likedComments: arrayOfOids,
	following: arrayOfOids,
	followers: arrayOfOids
});
export const User = mongoose.model('User', userSchema);

const postSchema = new Schema({
	user: Schema.Types.ObjectId,
	text: String,
	creationDate: { type: Date, default: Date.now },
	song: { type: String, default: null },
	comments: Schema.Types.Mixed,
	usersLikes: arrayOfOids
});
export const Post = mongoose.model('Post', postSchema);

const songSchema = new Schema({
	name: String,
	user: Schema.Types.ObjectId,
	creationDate: { type: Date, default: Date.now },
	song: Schema.Types.Mixed
});
export const Song = mongoose.model('Song', songSchema);
