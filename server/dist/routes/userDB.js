'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.editUser = exports.addUser = exports.unfollowUser = exports.followUser = exports.getUsersByQuery = exports.getUsersById = exports.getUserById = exports.getUser = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _models = require('./models');

var _properties = require('../properties');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getUser = exports.getUser = function getUser(username, password, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.User.find({ username: username, password: password }, function (err, users) {
			return callback(users.length == 0 ? null : users[0]);
		});
	});
};

var getUserById = exports.getUserById = function getUserById(id, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.User.find({ _id: id }, function (err, users) {
			return callback(users.length == 0 ? null : users[0]);
		});
	});
};

var getUsersById = exports.getUsersById = function getUsersById(ids, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.User.find(ids ? { _id: { $in: ids } } : {}, function (err, users) {
			return callback(users);
		});
	});
};

var getUsersByQuery = exports.getUsersByQuery = function getUsersByQuery(query, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.User.find({ username: { $regex: query, $options: 'i' } }, function (err, users) {
			return callback(users);
		});
	});
};

var followUser = exports.followUser = function followUser(idA, idB, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.User.find({ _id: idA }, function (err, usersA) {
			if (usersA.length == 0) return callback('lenA0');
			var userA = usersA[0];

			_models.User.find({ _id: idB }, function (err, usersB) {
				if (usersB.length == 0) return callback('lenB0');
				var userB = usersB[0];

				userA.following.push(userB._id);
				userB.followers.push(userA._id);

				userA.save(function (err, product) {
					if (err) {
						callback(err);
						return console.log(err);
					}
					userB.save(function (err, product) {
						if (err) {
							callback(err);
							return console.log(err);
						}
						callback(null);
					});
				});
			});
		});
	});
};

var unfollowUser = exports.unfollowUser = function unfollowUser(idA, idB, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.User.find({ _id: idA }, function (err, usersA) {
			if (usersA.length == 0) return callback('lenA0');
			var userA = usersA[0];

			_models.User.find({ _id: idB }, function (err, usersB) {
				if (usersB.length == 0) return callback('lenB0');
				var userB = usersB[0];

				var indexA = userA.following.indexOf(userB._id);
				var indexB = userB.followers.indexOf(userA._id);

				userA.following.splice(indexA, 1);
				userB.followers.splice(indexB, 1);

				userA.save(function (err, product) {
					if (err) {
						callback(err);
						return console.log(err);
					}
					userB.save(function (err, product) {
						if (err) {
							callback(err);
							return console.log(err);
						}
						callback(null);
					});
				});
			});
		});
	});
};

var addUser = exports.addUser = function addUser(email, username, password, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.User.find({ username: username }, function (err, users) {
			if (users.length != 0) return callback();
			var user = new _models.User({ username: username, email: email, password: password });
			user.save(function (err, product) {
				if (err) return console.log(err);
				callback();
			});
		});
	});
};

var editUser = exports.editUser = function editUser(id, changes, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.User.find({ _id: id }, function (err, users) {
			if (users.length == 0) return callback(false);

			var user = users[0];
			Object.assign(user, changes);

			user.save(function (err, product) {
				if (err) return console.log(err);
				callback(true);
			});
		});
	});
};