import express from 'express';
import * as jsonpack from 'jsonpack';

import * as UserDB from './userDB';
import * as MusicDB from './musicDB';
import { stat } from 'fs';

const router = express.Router();
export default router;

router.post('/me', (req, res) => {
	MusicDB.getSongs([req.user._id], songs => {
		let user = Object.assign({}, req.user._doc, { me: true, songs: songs.map(s => ({ name: s.name, id: s._id })) });
		res.json(user);
	});
});

router.post('/profile', (req, res) => {
	UserDB.getUserById(jsonpack.unpack(req.body).user, user => {
		user = user._doc;
		if (jsonpack.unpack(req.body).user + '' == req.user._id + '') user = Object.assign({}, user, { me: true });
		if (user.followers.map(u => u + '').indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollowing: true });
		if (user.following.map(u => u + '').indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollower: true });
		res.json(jsonpack.pack(user));
	});
});

router.post('/profiles', (req, res) => {
	let returnUsers = _users => {
		let users = [];
		for (let i = 0; i < _users.length; i++) {
			let user = _users[i]._doc;
			if (user._id + '' == req.user._id + '') user = Object.assign({}, user, { me: true });
			if (user.followers.map(u => u + '').indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollowing: true });
			if (user.following.map(u => u + '').indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollower: true });
			users.push(user);
		}
		res.json(jsonpack.pack(users));
	};

	if (jsonpack.unpack(req.body).query) UserDB.getUsersByQuery(jsonpack.unpack(req.body).query, returnUsers);
	else UserDB.getUsersById(jsonpack.unpack(req.body).users, returnUsers);
});
router.post('/edit-profile', (req, res) => UserDB.editUser(req.user._id, jsonpack.unpack(req.body), status => res.json(status)));
router.post('/follow', (req, res) => UserDB.followUser(req.user._id, jsonpack.unpack(req.body).user, error => res.json('fail' || 'success')));
router.post('/unfollow', (req, res) => UserDB.unfollowUser(req.user._id, jsonpack.unpack(req.body).user, error => res.json('fail' || 'success')));
