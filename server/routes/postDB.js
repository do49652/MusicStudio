import mongoose from 'mongoose';
import { Post } from './models';
import { mongodb } from '../properties';

export const getPosts = (users, skip, limit, callback) => {
	mongoose.connect(mongodb, () => {
		Post.find({ user: { $in: users } })
			.sort({ creationDate: -1 })
			.skip(skip)
			.limit(limit)
			.exec((err, posts) => callback(posts));
	});
};

export const getPost = (id, callback) => {
	mongoose.connect(mongodb, () => {
		Post.find({ _id: id }).exec((err, posts) => callback(posts.length == 0 ? null : posts[0]));
	});
};

export const addPost = (user, text, song, callback) => {
	mongoose.connect(mongodb, () => {
		let post = new Post({ user, text, song });
		post.save((err, product) => {
			if (err) console.log(err);
			callback(err == null);
		});
	});
};

export const updatePost = (post, callback) => {
	mongoose.connect(mongodb, () => {
		post.save((err, product) => {
			if (err) console.log(err);
			callback(err == null);
		});
	});
};
