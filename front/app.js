import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Link } from 'react-router-dom';

import Home from './pages/Home';
import ProfileList from './pages/ProfileList';
import Editor, { TrackEditor } from './pages/Editor';
import Profile from './pages/Profile';

const HomePage = () => {
	return <Home />;
};

const Search = ({ match }) => {
	return <ProfileList query={match.params.query} />;
};

const ProfilePage = ({ match }) => {
	return <Profile id={match.params.id} />;
};

const Followers = ({ match }) => {
	return <ProfileList id={match.params.id} page="Followers" />;
};

const Following = ({ match }) => {
	return <ProfileList id={match.params.id} page="Following" />;
};

const TrackEditorPage = ({ match }) => {
	return <TrackEditor id={match.params.id} />;
};

const EditorPage = () => {
	return <Editor />;
};

const App = () => {
	return (
		<BrowserRouter>
			<div>
				<Route exact path="/" component={HomePage} />
				<Route exact path="/search/:query" component={Search} />
				<Route exact path="/profile" component={ProfilePage} />
				<Route exact path="/profile/:id" component={ProfilePage} />
				<Route exact path="/profile/:id/followers" component={Followers} />
				<Route exact path="/profile/:id/following" component={Following} />
				<Route exact path="/editor" component={EditorPage} />
				<Route exact path="/editor/:id" component={TrackEditorPage} />
			</div>
		</BrowserRouter>
	);
};

ReactDOM.render(<App />, document.getElementById('app'));
