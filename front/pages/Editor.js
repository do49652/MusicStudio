import React from 'react';

import Header from './elements/Header';
import FileManager from './elements/FileManager';
import * as jsonpack from 'jsonpack';

import Section from '../studio/audio/Section';
import SectionEditor from '../studio/editor/SectionEditor';
import Instruments from '../studio/audio/Instuments';
import { Button, PlayerButton, PlayerRoundButton, PlayerSectionEditor, PlayerTrackEditor, PlayerEffectsEditor } from '../studio/editor/elements/Player';

const headers = {
	Accept: 'application/json',
	'Content-Type': 'text/plain'
};

export default class Editor extends React.Component {
	constructor(...args) {
		super(...args);
	}
	render() {
		return (
			<div>
				<Header title={'Music Studio'} currentPage={'Editor'} />
				<br />
				<div className="container">
					<div className="row">
						<FileManager />
					</div>
				</div>
			</div>
		);
	}
}

export class TrackEditor extends React.Component {
	constructor(...args) {
		super(...args);
		this.state = {
			ready: false,
			shownSection: -1,
			song: null,
			trackSection: null,
			name: { hasError: false, value: 'Name' },
			type: null,
			sounds: [],
			trackSectionEditor: null,
			saveText: 'Save',
			fullscreen: false
		};
		this.sections = [];
		this.sectionEditors = [];

		this.addNew = this.addNew.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.setTrackSectionEditor = this.setTrackSectionEditor.bind(this);
		this.save = this.save.bind(this);
	}
	setTrackSectionEditor(tse) {
		this.setState({ trackSectionEditor: tse });
	}
	componentDidMount() {
		let body = jsonpack.pack({ id: this.props.id });
		fetch(new Request('/get-song', { method: 'post', headers, credentials: 'same-origin', body }))
			.then(res => res.text())
			.then(res => {
				this.setState({ song: jsonpack.unpack(res) }, () => {
					let song = this.state.song;
					if (song == null) return;
					document.title = song.name;
					song = song.song;

					let track = new Section(song.track);

					this.setState({ trackSection: new Section(song.track) }, () =>
						this.state.trackSection.loadTrack(() => {
							let loader = i => {
								if (song.sections.length > i + 1) this.addNew(() => loader(i + 1), song.sections[i]);
								else this.addNew(() => this.setState({ ready: true }), song.sections[i]);
							};

							if (song.sections.length > 0) loader(0);
							else this.setState({ ready: true });
						})
					);
				});
			});
	}
	handleChange(event) {
		let { name, value } = event.target;

		if (name == 'section-type') {
			this.setState({
				type: value,
				sounds: [
					value == 'sound'
						? Instruments.Sounds.sounds[0]
						: value == 'drums'
							? Instruments.Drums.sounds[0]
							: Object.values(Instruments)
									.map(i => i.name)
									.filter(i => i != 'GetNoteName' && i != 'Drums' && i != 'Sounds')[0]
				]
			});
		}

		if (name == 'name') {
			this.setState({
				name: {
					hasError: !/^[A-Za-z0-9]*$/.test(value),
					value
				}
			});
		}

		if (event.target.options) {
			let sounds = [];
			for (let i = 0; i < event.target.options.length; i++) {
				if (event.target.options[i].selected) sounds.push(event.target.options[i].value);
			}
			this.setState({ sounds });
		}
	}
	addNew(callback, insertSection) {
		if (!({}.toString.call(callback) === '[object Function]')) {
			callback = null;
			insertSection = null;
		}

		if (this.state.type == 'sound') {
			this.state.trackSection.newSound(this.state.sounds[0], 'sounds', this.state.sounds[0], null, this.state.trackSectionEditor.sectionEditor);
		} else if (this.state.type == 'drums' || this.state.type == 'instrument' || insertSection) {
			let song = this.state.song;
			let section = null;
			if (insertSection) {
				section = new Section(insertSection);
			} else {
				section = new Section(
					this.state.name.value,
					this.state.type == 'drums'
						? { name: 'Drums', dir: 'drums', sounds: this.state.sounds }
						: Object.values(Instruments)[
								Object.values(Instruments)
									.map(i => i.name)
									.indexOf(this.state.sounds[0])
						  ],
					{ volume: 1.0, pitch: 0, bpm: song.song.bpm, duration: 1 },
					song.song.track.columns,
					new Buffer(this.state.name.value + '_' + new Date().getTime() + '_' + this.state.type).toString()
				);
			}

			let sectionData = section.save();
			this.sections.push(section);
			if (!insertSection) song.song.sections.push(sectionData);
			this.state.trackSection.tracks.push(sectionData);

			this.sections[this.sections.length - 1].loadTrack(() =>
				this.setState({ song }, () => {
					if (!insertSection) {
						this.sections[this.sections.length - 1].liveTrack.compile(() => {
							this.state.trackSection.newSound(
								this.sections[this.sections.length - 1].name,
								'custom/' + this.state.song.user,
								this.sections[this.sections.length - 1].id,
								null,
								this.state.trackSectionEditor.sectionEditor,
								callback
							);
						});
					} else {
						callback();
					}
					this.sections[this.sections.length - 1].liveTrack.onCompiling = cb => {
						if (this.state.trackSection.liveTrack.buffer) {
							if (this.state.trackSection.liveTrack.buffer.urls.length < this.sections.length) return cb();
							this.state.trackSection.liveTrack.buffer.reloadSound(this.sections.length - 1, () =>
								this.state.trackSection.liveTrack.load(this.sections.length - 1, 0, cb)
							);
						} else cb();
					};
				})
			);
		}
	}
	save(a) {
		this.setState({ saveText: 'Saving...' }, () => {
			if (this.state.shownSection != -1) {
				this.sections[this.state.shownSection].liveTrack.compile(() => {
					if (a == 'Back') {
						this.setState({ shownSection: -1 }, () => {
							this.state.trackSection.tracks = [];
							this.sections.map(s => this.state.trackSection.tracks.push(s));
							this.state.trackSection.liveTrack.compile(() => {
								this.effectEditor.reload();
								this.setState({ saveText: 'Save' });
							});
						});
					} else {
						this.effectEditor.reload();
						this.state.trackSection.tracks = [];
						this.sections.map(s => this.state.trackSection.tracks.push(s));
						this.state.trackSection.liveTrack.compile(() => {
							this.setState({ saveText: 'Save' });
						});
					}
				});
			} else {
				this.state.trackSection.tracks = [];
				this.sections.map(s => this.state.trackSection.tracks.push(s));
				this.state.trackSection.liveTrack.compile(() => {
					this.effectEditor.reload();
					this.setState({ saveText: 'Save' });
				});
			}
		});
	}
	render() {
		return (
			<div>
				<Header title={'Music Studio'} currentPage={'Editor'} />
				<div className="container-fluid">
					<div className="text-center">
						<div className="btn-group" role="group">
							<button className="btn btn-success btn-raised" style={{ marginTop: '5px' }} onClick={() => this.setState({ fullscreen: !this.state.fullscreen })}>
								Fullscreen
							</button>
							{(this.state.saveText == 'Save' && (
								<div className="btn-group" role="group" style={{ padding: 0, margin: 0 }}>
									{this.state.shownSection != -1 && (
										<button className={'btn btn-danger btn-raised'} style={{ marginTop: '5px' }} onClick={() => this.save('Back')}>
											Back
										</button>
									)}
									<button className={'btn btn-primary btn-raised'} style={{ marginTop: '5px' }} onClick={this.save}>
										Save
									</button>
								</div>
							)) || (
								<button className="btn btn-primary btn-raised" style={{ marginTop: '5px' }} disabled>
									Saving...
								</button>
							)}
						</div>
					</div>
					<div className="row">
						<div className={'col-md-' + (this.state.fullscreen ? 12 : 6)}>
							{this.state.trackSection && (
								<PlayerTrackEditor
									className={this.state.shownSection != -1 ? 'hidden' : ''}
									ref={pse => {
										if (!pse) return;
										!this.state.trackSectionEditor && this.setTrackSectionEditor(pse);
										pse.sectionEditor.setState({ editor: this });
									}}
									section={this.state.trackSection}
									offset={0}
									add={() => $('#new-section').modal({ backdrop: false })}
									onSave={() => {
										this.state.trackSection.tracks = [];
										this.sections.map(s => this.state.trackSection.tracks.push(s));
									}}
									editor={this}
								/>
							)}
							{this.state.ready &&
								this.state.trackSectionEditor &&
								this.state.song.song.sections.length > 0 &&
								this.state.song.song.sections.map(s => (
									<PlayerSectionEditor
										className={this.state.shownSection != this.sections.map(ss => ss.id).indexOf(s.id) ? 'hidden' : ''}
										key={s.id}
										ref={pse => {
											if (!this.sections[this.sections.map(ss => ss.id).indexOf(s.id)] || !pse) return;
											this.sectionEditors[this.sections.map(ss => ss.id).indexOf(s.id)] = pse;
											this.sections[this.sections.map(ss => ss.id).indexOf(s.id)].sectionEditor = pse;
										}}
										section={this.sections[this.sections.map(ss => ss.id).indexOf(s.id)]}
										offset={this.sections.map(ss => ss.id).indexOf(s.id) + 1}
										onSave={() => {
											this.effectEditor.reload();
										}}
										editor={this}
									/>
								))}
						</div>

						{this.state.song && (
							<div className={'col-md-' + (this.state.fullscreen ? 12 : 6)}>
								{this.state.shownSection == -1 && (
									<PlayerEffectsEditor
										ref={pee => (this.effectEditor = pee)}
										url={'../instruments/custom/' + this.state.song.user + '/' + this.state.song.song.track.id + '.wav'}
										section={this.state.trackSection}
										onSave={() => {
											this.state.trackSection.tracks = [];
											this.sections.map(s => this.state.trackSection.tracks.push(s));
										}}
									/>
								)}
								{this.state.shownSection != -1 && (
									<PlayerEffectsEditor
										ref={pee => (this.effectEditor = pee)}
										url={'../' + this.state.song.song.track.sounds[this.state.shownSection].sound}
										section={this.sections[this.state.shownSection]}
									/>
								)}
							</div>
						)}
					</div>
				</div>

				<div id="new-section" className="modal fade" tabIndex="-1" role="dialog">
					<div className="modal-dialog modal-lg modal-dialog-centered" role="document">
						<div className="modal-content">
							<div className="modal-header">
								<h5 className="modal-title" id="new-song">
									Add new track
								</h5>
								<button type="button" className="close" data-dismiss="modal">
									<span>&times;</span>
								</button>
							</div>

							<div className="modal-body">
								{['Instrument', 'Drums', 'Sound'].map(t => (
									<div key={t} className="radio">
										<label className="text-dark">
											<input type="radio" name="section-type" value={t.toLowerCase()} onChange={this.handleChange} />
											<span className="bmd-radio">
												<div className="ripple-container" />
											</span>
											{t}
										</label>
									</div>
								))}

								{this.state.type == 'instrument' && (
									<div>
										<fieldset className={'form-group' + (this.state.name.hasError ? ' has-danger' : '')}>
											<label htmlFor="nameInput1" className="bmd-label-floating">
												Name
											</label>

											<input
												type="text"
												name="name"
												className={'form-control' + (this.state.name.hasError ? ' form-control-danger' : '')}
												id="nameInput1"
												onChange={this.handleChange}
											/>

											{this.state.name.hasError && <div className="form-control-feedback">Use only alphanumeric characters.</div>}
											<small className="bmd-help">Use only alphanumeric characters.</small>
										</fieldset>

										<fieldset className="form-group">
											<label htmlFor="instument-choice">Choose an instrument</label>
											<select className="form-control" id="instument-choice" onChange={this.handleChange}>
												{Object.values(Instruments)
													.map(i => i.name)
													.filter(i => i != 'GetNoteName' && i != 'Drums' && i != 'Sounds')
													.map(i => <option key={i}>{i}</option>)}
											</select>
										</fieldset>
									</div>
								)}

								{this.state.type == 'sound' && (
									<fieldset className="form-group">
										<label htmlFor="sound-choice">Choose a sound</label>
										<select className="form-control" id="sound-choice" onChange={this.handleChange}>
											{Instruments.Sounds.sounds.map(i => <option key={i}>{i}</option>)}
										</select>
									</fieldset>
								)}

								{this.state.type == 'drums' && (
									<div>
										<fieldset className={'form-group' + (this.state.name.hasError ? ' has-danger' : '')}>
											<label htmlFor="nameInput2" className="bmd-label-floating">
												Name
											</label>

											<input
												type="text"
												name="name"
												className={'form-control' + (this.state.name.hasError ? ' form-control-danger' : '')}
												id="nameInput2"
												onChange={this.handleChange}
											/>

											{this.state.name.hasError && <div className="form-control-feedback">Use only alphanumeric characters.</div>}
											<small className="bmd-help">Use only alphanumeric characters.</small>
										</fieldset>

										<fieldset className="form-group">
											<label htmlFor="drums-choice">
												Choose sounds{' '}
												<small>
													<i>(Hold CTRL to select multiple sounds)</i>
												</small>
											</label>
											<select
												multiple
												className="form-control"
												id="drums-choice"
												style={{
													maxHeight: '1000px',
													overflowY: 'auto',
													overflowX: 'hidden'
												}}
												onChange={this.handleChange}
											>
												{Instruments.Drums.sounds.map(i => <option key={i}>{i}</option>)}
											</select>
										</fieldset>
									</div>
								)}
							</div>

							<div className="modal-footer">
								<button type="button" className="btn btn-secondary" data-dismiss="modal">
									Cancel
								</button>
								<button
									type="button"
									className="btn btn-primary"
									onClick={() =>
										this.addNew(() => {
											if (!this.state.song.song.track.sounds) this.state.song.song.track.sounds = [];
											let sec = this.state.song.song.sections[this.state.song.song.track.sounds.length];
											this.state.song.song.track.sounds.push({
												name: sec.name,
												properties: this.state.song.song.track.properties,
												sound: 'instruments/custom/' + this.state.song.user + '/' + sec.id + '.wav'
											});
										}, null)
									}
									data-dismiss="modal"
								>
									Add
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}
