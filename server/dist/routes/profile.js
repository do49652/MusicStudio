'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _jsonpack = require('jsonpack');

var jsonpack = _interopRequireWildcard(_jsonpack);

var _userDB = require('./userDB');

var UserDB = _interopRequireWildcard(_userDB);

var _musicDB = require('./musicDB');

var MusicDB = _interopRequireWildcard(_musicDB);

var _fs = require('fs');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express2.default.Router();
exports.default = router;


router.post('/me', function (req, res) {
	MusicDB.getSongs([req.user._id], function (songs) {
		var user = Object.assign({}, req.user._doc, { me: true, songs: songs.map(function (s) {
				return { name: s.name, id: s._id };
			}) });
		res.json(user);
	});
});

router.post('/profile', function (req, res) {
	UserDB.getUserById(jsonpack.unpack(req.body).user, function (user) {
		user = user._doc;
		if (jsonpack.unpack(req.body).user + '' == req.user._id + '') user = Object.assign({}, user, { me: true });
		if (user.followers.map(function (u) {
			return u + '';
		}).indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollowing: true });
		if (user.following.map(function (u) {
			return u + '';
		}).indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollower: true });
		res.json(jsonpack.pack(user));
	});
});

router.post('/profiles', function (req, res) {
	var returnUsers = function returnUsers(_users) {
		var users = [];
		for (var i = 0; i < _users.length; i++) {
			var user = _users[i]._doc;
			if (user._id + '' == req.user._id + '') user = Object.assign({}, user, { me: true });
			if (user.followers.map(function (u) {
				return u + '';
			}).indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollowing: true });
			if (user.following.map(function (u) {
				return u + '';
			}).indexOf(req.user._id + '') != -1) user = Object.assign({}, user, { meFollower: true });
			users.push(user);
		}
		res.json(jsonpack.pack(users));
	};

	if (jsonpack.unpack(req.body).query) UserDB.getUsersByQuery(jsonpack.unpack(req.body).query, returnUsers);else UserDB.getUsersById(jsonpack.unpack(req.body).users, returnUsers);
});
router.post('/edit-profile', function (req, res) {
	return UserDB.editUser(req.user._id, jsonpack.unpack(req.body), function (status) {
		return res.json(status);
	});
});
router.post('/follow', function (req, res) {
	return UserDB.followUser(req.user._id, jsonpack.unpack(req.body).user, function (error) {
		return res.json('fail' || 'success');
	});
});
router.post('/unfollow', function (req, res) {
	return UserDB.unfollowUser(req.user._id, jsonpack.unpack(req.body).user, function (error) {
		return res.json('fail' || 'success');
	});
});