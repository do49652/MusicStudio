'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.saveSong = exports.deleteSong = exports.addSong = exports.getSongsById = exports.getSongs = exports.getSong = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _models = require('./models');

var _properties = require('../properties');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getSong = exports.getSong = function getSong(id, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.Song.find({ _id: id }, function (err, songs) {
			if (songs.length == 0) return callback(null);
			callback(songs[0]);
		});
	});
};

var getSongs = exports.getSongs = function getSongs(users, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.Song.find({ user: { $in: users } }).sort({ creationDate: -1 }).exec(function (err, songs) {
			return callback(songs);
		});
	});
};

var getSongsById = exports.getSongsById = function getSongsById(ids, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.Song.find({ _id: { $in: ids } }).sort({ creationDate: -1 }).exec(function (err, songs) {
			return callback(songs);
		});
	});
};

var addSong = exports.addSong = function addSong(user, name, song, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		var music = new _models.Song({ name: name, user: user, song: song });
		music.save(function (err, product) {
			if (err) console.log(err);
			callback(err || product._id);
		});
	});
};

var deleteSong = exports.deleteSong = function deleteSong(id, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.Song.remove({ _id: id }).exec(function (err) {
			return callback(err);
		});
	});
};

var saveSong = exports.saveSong = function saveSong(user, _song, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.Song.find({ user: user }, function (err, songs) {
			if (songs.length == 0) return callback(false);
			songs = songs.filter(function (s) {
				return s.song.track.id + '' == _song.song.id + '';
			});
			if (songs.length == 0) return callback(false);

			var song = songs[0];
			Object.assign(song, { song: { bpm: song.song.bpm, bars: song.song.bars, beats: song.song.beats, sections: _song.sections, track: _song.song } });

			song.save(function (err, product) {
				if (err) return console.log(err);
				callback(true);
			});
		});
	});
};