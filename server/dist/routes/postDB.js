'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.updatePost = exports.addPost = exports.getPost = exports.getPosts = undefined;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _models = require('./models');

var _properties = require('../properties');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var getPosts = exports.getPosts = function getPosts(users, skip, limit, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.Post.find({ user: { $in: users } }).sort({ creationDate: -1 }).skip(skip).limit(limit).exec(function (err, posts) {
			return callback(posts);
		});
	});
};

var getPost = exports.getPost = function getPost(id, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		_models.Post.find({ _id: id }).exec(function (err, posts) {
			return callback(posts.length == 0 ? null : posts[0]);
		});
	});
};

var addPost = exports.addPost = function addPost(user, text, song, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		var post = new _models.Post({ user: user, text: text, song: song });
		post.save(function (err, product) {
			if (err) console.log(err);
			callback(err == null);
		});
	});
};

var updatePost = exports.updatePost = function updatePost(post, callback) {
	_mongoose2.default.connect(_properties.mongodb, function () {
		post.save(function (err, product) {
			if (err) console.log(err);
			callback(err == null);
		});
	});
};