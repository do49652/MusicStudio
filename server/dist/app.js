'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _expressSession = require('express-session');

var _expressSession2 = _interopRequireDefault(_expressSession);

var _login = require('./routes/login');

var _login2 = _interopRequireDefault(_login);

var _profile = require('./routes/profile');

var _profile2 = _interopRequireDefault(_profile);

var _posts = require('./routes/posts');

var _posts2 = _interopRequireDefault(_posts);

var _music = require('./routes/music');

var _music2 = _interopRequireDefault(_music);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
app.use(_express2.default.static('public'));
app.set('views', __dirname + '/../views');
app.set('view engine', 'pug');

app.use(_bodyParser2.default.urlencoded({ extended: false }));
app.use(_bodyParser2.default.text());
app.use((0, _expressSession2.default)({
	secret: 'secret',
	saveUninitialized: true,
	resave: true
}));
app.use(_passport2.default.initialize());
app.use(_passport2.default.session());

/*
app.get('*.js', (req, res, next) => {
	req.url = req.url + '.gz';
	res.set('Content-Encoding', 'gzip');
	next();
});*/

var auth = function auth(req, res, next) {
	if (req.isAuthenticated()) return next();
	res.redirect('/login');
};
var render = function render(req, res) {
	return res.render('index', { title: 'Music Studio' });
};

app.use(_login2.default);
app.use(_profile2.default);
app.use(_posts2.default);
app.use(_music2.default);
app.get('*', auth, render);

app.listen(process.env.PORT || 3000, console.log('\n\nServer is ready...'));